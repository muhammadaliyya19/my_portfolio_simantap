###################
Sistem Informasi Monitoring & Evaluasi Akreditasi Pengadilan Tinggi
###################

**Bahasa Pemrograman** : PHP 7

**Framework** : Codeigniter 3.11

**UI Theme** : Admin LTE

*********
------ List Modul ------
*********

1. Auth (Login, Logout)
2. Dashboard
3. Referensi 

   - Kelola Data Pegawai

   - Kelola Data Pengguna

4. Validasi Berkas untuk setiap area.
5. Pemantauan untuk setiap area \
6. Laporan
7. Profil Pengguna

*********
------ Cara run app local ------
*********

Akun login : username = admin | password = admin

1. Download atau clone repo ini

2. Buka htdocs dan buat folder bernama "si-mantap"

3. Extract project didalam folder si-mantap.

4. Run local server

5. Buat database bernama "si-mantap" dan import file sql yang disediakan

6. Lakukan penyesuaian file database.php pada CodeIgniter jika password, username dan hostname anda tidak default

7. Akses melalui browser dengan membuka localhost/si-mantap

Terima kasih.
