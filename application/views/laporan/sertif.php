 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"><?=$title; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">Si-Mantap</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('referensi/pegawai'); ?>"><?=$title ?></a></li>              
              <li><a href="#" class="btn btn-sm btn-primary ml-2 add_pegawai" data-toggle="modal" data-target="#modal-pegawai">+ Pegawai</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <table id="example1" class="table table-hover">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>INSTANSI</th>
                    <th>OPSI</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>INSTANSI</th>
                    <th>OPSI</th>
                  </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; foreach ($sertif as $s):?>
                    <tr>
                      <td><?=$i; ?></td>
                      <td><?=$s['nama']; ?></td>                      
                      <td><?=$s['instansi']; ?></td>                      
                      <td>
                        <form action="<?=base_url('laporan/downloads') ?>" method="post" target="_blank">
                          <input type="hidden" name="nama" value="<?=$s['nama']; ?>">
                          <input type="hidden" name="instansi" value="<?=$s['instansi']; ?>">
                          <button type="submit" class="btn btn-success float-right mt-2"><i class="fas fa-download"></i></button>
                        </form>
                      </td>                      
                    </tr>                    
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
  </div>
