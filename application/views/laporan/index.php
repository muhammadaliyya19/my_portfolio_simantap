  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"><?=$title; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('laporan'); ?>"><?=$title ?></a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
          <form action="<?=base_url('laporan/download'); ?>" method="post">
            <div class="card">      
              <div class="card-header" align="center">
                <h5>UNDUH LAPORAN MONEV AKREDITASI PENJAMINAN MUTU PENGADILAN TINGGI BANTEN</h5>
              </div>        
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <div class="row">
                      <div class="col" align="center">
                        <span class="h5">Silakan masukkan keterangan sebelum anda mengunduh laporan.</span>
                      </div>
                    </div>
                    <div class="row my-3">
                      <div class="col">Pada hari ini </div>
                      <div class="col">
                        <select  class="form-control" name="hari">
                          <option value="">Hari</option>
                          <option value="Senin" <?php if(date('l') == 'Monday'){echo "selected";} ?>>Senin</option>
                          <option value="Selasa" <?php if(date('l') == 'Tuesday'){echo "selected";} ?>>Selasa</option>
                          <option value="Rabu" <?php if(date('l') == 'Wednesday'){echo "selected";} ?>>Rabu</option>
                          <option value="Kamis" <?php if(date('l') == 'Thursday'){echo "selected";} ?>>Kamis</option>
                          <option value="Jum'at" <?php if(date('l') == 'Friday'){echo "selected";} ?>>Jum'at</option>
                          <option value="Jum'at" <?php if(date('l') == 'Saturday'){echo "selected";} ?>>Sabtu</option>
                        </select>
                      </div>
                      <div class="col">
                        <input type="date" name="tanggal" class="form-control" value="<?=date('Y-m-d'); ?>">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">Untuk bulan / tahun</div>
                      <div class="col">
                        <select name="bulan" class="form-control">
                          <option value="">Bulan</option>
                          <option value="Januari">Januari</option>
                          <option value="Februari">Februari</option>
                          <option value="Maret">Maret</option>
                          <option value="April">April</option>
                          <option value="Mei">Mei</option>
                          <option value="Juni">Juni</option>
                          <option value="Juli">Juli</option>
                          <option value="Agustus">Agustus</option>
                          <option value="September">September</option>
                          <option value="Oktober">Oktober</option>
                          <option value="November">November</option>
                          <option value="Desember">Desember</option>
                        </select>
                      </div>
                      <div class="col">
                        <select name="tahun" class="form-control">
                          <option value="">Tahun</option>
                          <?php for ($i=2020; $i <= 2030; $i++) { ?>
                            <option value="<?=$i;?>"><?=$i;?></option>
                          <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-12 mt-3">
                    <div class="row">
                      <div class="col-4">Catatan</div>
                      <div class="col">
                        <textarea name="catatan" class="form-control" rows="5" placeholder="Catatan monitoring evaluasi . . ."></textarea>
                      </div>
                    </div>
                </div>  
              </div>                  
                <button type="submit" class="btn btn-success float-right mt-2">Unduh Laporan</button>
            </div>
                  
            </div>
          </form>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper