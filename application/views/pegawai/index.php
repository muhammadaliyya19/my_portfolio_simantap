  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"><?=$title; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">Si-Mantap</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('referensi/pegawai'); ?>"><?=$title ?></a></li>              
              <li><a href="#" class="btn btn-sm btn-primary ml-2 add_pegawai" data-toggle="modal" data-target="#modal-pegawai">+ Pegawai</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <table id="example1" class="table">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA</th>
                    <th>GOLONGAN / RUANG</th>
                    <th>PANGKAT</th>
                    <th>AREA</th>
                    <th>JABATAN</th>
                    <th>AKTIF</th>
                    <th>OPSI</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>NO</th>
                    <th>NIP</th>
                    <th>NAMA</th>
                    <th>GOLONGAN / PANGKAT</th>
                    <th>PANGKAT</th>
                    <th>AREA</th>
                    <th>JABATAN</th>
                    <th>AKTIF</th>
                    <th>OPSI</th>
                  </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; foreach ($pegawai as $p):?>
                    <tr>
                      <td><?=$i; ?></td>
                      <td><?=$p['NIP']; ?></td>
                      <td><?=$p['nama']; ?></td>
                      <td><?=$p['golongan'] . ' / (' . $p['ruang']. ')'; ?></td>
                      <td><?=$p['pangkat']; ?></td>
                      <td><?=$p['area']; ?></td>
                      <td><?=$p['nama_jabatan']; ?></td>
                      <td><?=$p['aktif']; ?></td>
                      <td>
                        <a href="#" class="edit_pegawai" data-toggle="modal" data-target="#modal-pegawai" data-id="<?=$p['id']; ?>"><span class="btn btn-xs btn-outline-success"><i class="fas fa-pencil-alt"></i></span></a>
                        <a href="<?=base_url('referensi/hapusPegawai/'.$p['id']); ?>" onclick="return confirm('Hapus pegawai?')"><span class="btn btn-xs btn-outline-danger"><i class="fas fa-trash"></i></span></a>
                      </td>                      
                    </tr>                    
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <!-- MODAL PEGAWAI -->
  <div class="modal fade" id="modal-pegawai">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Pegawai</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body modal-pegawai">
                <form action="<?= base_url('referensi/tambahpegawai'); ?>" method="post">
                  <input type="hidden" class="form-control" id="id" name="id">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        NIP
                      </div>
                      <div class="col-10">
                        <input type="number" class="form-control" id="nip" placeholder="NIP . . . " name="nip">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Nama Gelar
                      </div>
                      <div class="col-10">
                        <input type="text" class="form-control" id="namapegawai" placeholder="Nama pegawai . . . " name="nama">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Golongan
                      </div>
                      <div class="col-10">
                        <select name="golongan" id="golongan" class="form-control">
                          <option value="">Pilih Golongan</option>
                          <option value="I">I</option>
                          <option value="II">II</option>
                          <option value="III">III</option>
                          <option value="IV">IV</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Ruang
                      </div>
                      <div class="col-10">
                        <select name="ruang" id="ruang" class="form-control">
                          <option value="">Pilih Ruang</option>
                          <option value="a">a</option>
                          <option value="b">b</option>
                          <option value="c">c</option>
                          <option value="d">d</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Pangkat
                      </div>
                      <div class="col-10">
                        <select name="pangkat" id="pangkat" class="form-control">
                          <option value="">Pilih Pangkat</option>
                          <option value="Juru Muda">Juru Muda</option>
                          <option value="Juru Muda Tingkat I">Juru Muda Tingkat I</option>
                          <option value="Juru">Juru</option>
                          <option value="Juru Tingkat I">Juru Tingkat I</option>
                          <option value="Pengatur Muda">Pengatur Muda</option>
                          <option value="Pengatur Muda Tingkat I">Pengatur Muda Tingkat I</option>
                          <option value="Pengatur">Pengatur</option>
                          <option value="Pengatur Tingkat I">Pengatur Tingkat I</option>
                          <option value="Penata Muda">Penata Muda</option>
                          <option value="Penata Muda Tingkat I">Penata Muda Tingkat I</option>
                          <option value="Penata">Penata</option>
                          <option value="Penata Tingkat I">Penata Tingkat I</option>
                          <option value="Pembina">Pembina</option>
                          <option value="Pembina Tingkat I">Pembina Tingkat I</option>
                          <option value="Pembina Utama Muda">Pembina Utama Muda</option>
                          <option value="Pembina Utama Madya">Pembina Utama Madya</option>
                          <option value="Pembina Utama">Pembina Utama</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Area
                      </div>
                      <div class="col-10">
                        <select name="area" id="area" class="form-control">
                          <option value="">Pilih Area Kerja</option>
                          <?php foreach ($area as $a) :?>
                            <option value="<?=$a['id']; ?>"><?=$a['nama']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Jabatan
                      </div>
                      <div class="col-10">
                        <select name="jabatan" id="jabatan" class="form-control">
                          <option value="">Pilih Jabatan</option>
                          <?php foreach ($jabatan as $j) :?>
                            <option value="<?=$j['id']; ?>"><?=$j['nama_jabatan']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Alamat
                      </div>
                      <div class="col-10">
                        <input type="text" class="form-control" id="alamat" placeholder="Alamat . . . " name="alamat">
                      </div>
                    </div>
                  </div>                  
                  <div class="form-group">                  
                    <div class="row">
                      <div class="col-2">
                        Aktif
                      </div>
                      <div class="col-10" >
                        <select name="keaktifan" id="keaktifan" class="form-control">
                          <option value="">Pilih Status Keaktifan</option>
                          <option value="Ya">Ya</option>
                          <option value="Tidak">Tidak</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success float-right ml-2 tbhpegawaibtn">Simpan</button>
                  <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Batal</button>
                </form>
            </div>
          </div>            
        </div>
          <!-- /.modal-content -->
        </div>
  </div>
  <!-- /.content-wrapper