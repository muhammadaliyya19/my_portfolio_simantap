  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-8">
            <h4 class="m-0 text-dark"><?=$title;?></h4>
          </div><!-- /.col -->
          <div class="col-sm-4">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('user/pemantauan'); ?>"><?=$title ?></a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <form action="" method="post">
                  <div class="row">
                    <span class="h4">Validasi Bulan <?php if($bulan != ''){echo $bulan .' '. $tahun;} else {echo date('F') .' '.$tahun;} ?></span>                
                    <div class="col"></div>                  
                    <div class="col-2">
                      <select name="bulan" id="bulan" class="form-control">
                        <option value="" <?php if ($bulan != '') {echo " selected";} ?>>-- Pilih --</option>
                        <option value="1" <?php if ($bulan == 'January') {echo " selected";} ?>>Januari</option>
                        <option value="2" <?php if ($bulan == 'February') {echo " selected";} ?>>Februari</option>
                        <option value="3" <?php if ($bulan == 'March') {echo " selected";} ?>>Maret</option>
                        <option value="4" <?php if ($bulan == 'April') {echo " selected";} ?>>April</option>
                        <option value="5" <?php if ($bulan == 'May') {echo " selected";} ?>>Mei</option>
                        <option value="6" <?php if ($bulan == 'June') {echo " selected";} ?>>Juni</option>
                        <option value="7" <?php if ($bulan == 'July') {echo " selected";} ?>>Juli</option>
                        <option value="8" <?php if ($bulan == 'August') {echo " selected";} ?>>Agustus</option>
                        <option value="9" <?php if ($bulan == 'September') {echo " selected";} ?>>September</option>
                        <option value="10" <?php if ($bulan == 'October') {echo " selected";} ?>>Oktober</option>
                        <option value="11" <?php if ($bulan == 'November') {echo " selected";} ?>>November</option>
                        <option value="12" <?php if ($bulan == 'December') {echo " selected";} ?>>Desember</option>
                      </select>
                    </div>
                    <div class="col-lg-1">                  
                      <select name="tahun" id="tahun" class="form-control">
                        <?php for ($i=2020; $i < 2030; $i++) { ?>
                          <option value="<?=$i; ?>" <?php if (date("Y") == $i) {echo " selected";} ?>><?=$i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-lg-1">
                      <button type="submit" class="btn btn-success" style="width: 100%;">Show</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="card-body">
                <table id="example1" class="table">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>AREA</th>
                    <th>PENILAIAN</th>
                    <th>EVIDENCE</th>
                    <th>STATUS</th>
                    <th>TGL UPLOAD</th>
                    <th>TGL UPDATE</th>
                    <th>OPSI</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>NO</th>
                    <th>AREA</th>
                    <th>PENILAIAN</th>
                    <th>EVIDENCE</th>
                    <th>STATUS</th>
                    <th>TGL UPLOAD</th>
                    <th>TGL UPDATE</th>
                    <th>OPSI</th>
                  </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; foreach ($penilaian as $p):?>
                    <tr>
                      <td scope="col" style="width: 1rem;"><?=$i; ?></td>
                      <td scope="col" style="width: 2rem;"><?=$p['area']; ?></td>
                      <td scope="col" style="width: 1rem;">
                        <?=$p['nama']."<br>"; ?>                        
                      </td>
                      <td>
                        <div style="width: 4rem; overflow-x: hidden;">                        
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 50rem;">
                              - <a href="#" class="showdoc" data-toggle="modal" data-target="#viewdoc" data-id="<?=$e['id']; ?>" data-docname="<?=$e['file']; ?>"><?= $e['file'].'<br>';?></a>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 10rem;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>                            
                            <div class="card my-2" style="width: 10rem;">
                            <?php if($e['status'] == "Pending"){ ?>                              
                                <div class="card-body text-primary">
                            <?php }else if($e['status'] == "Revisi"){ ?>
                                <div class="card-body text-danger">
                            <?php }else{ ?>
                                <div class="card-body text-success">
                            <?php } ?>                                  
                                  <p class="card-title"><b><?= $e['status'];?></b></p>
                                  <p class="card-text"><?=$e['keterangan']; ?></p>
                                </div>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_uploaded'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 4.5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_updated'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td scope="col" style="max-width: 5rem;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <a href="#" class="validasi_evidence" data-toggle="modal" data-target="#modal-evidence" data-area="<?=$p['area']; ?>" data-id="<?=$e['id']; ?>"><span class="btn btn-xs btn-outline-success" style="width: 100%;">Validasi</span></a>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </td>                      
                    </tr>                    
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
            </div>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </div>
  <!-- /.content-wrapper-->
        <!-- MODAL EVIDENCE -->
        <div class="modal fade" id="modal-evidence">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Validasi Evidence</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal-evidence">
                <div>
                  <?php echo form_open_multipart('pemantauan/validasiEvidence')?>
                    <input type="hidden" name="area" id="evi_area">
                    <input type="hidden" name="validasi_all" value="true">
                    <input type="hidden" name="evidence_id" id="evidence_id">
                    <div class="row">
                      <div class="col-3">Status</div>
                      <div class="col-9">
                        <select class="form-control" name="status" id="status">
                          <option value="Diterima">Diterima</option>
                          <option value="Revisi">Perlu Revisi</option>
                        </select>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-3">Keterangan</div>
                      <div class="col-9"><textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea></div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success float-right ml-2 tbhpegawaibtn">Simpan</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Batal</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        

        <!-- MODAL PREVIEW EVIDENCE -->
        <div class="modal fade" id="viewdoc">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Preview Dokumen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <iframe style="width:100%; height:700px;" frameborder="0" src=""></iframe>  
            </div>
          </div>
        </div>
    <!-- /.content -->    
  </div>
  

