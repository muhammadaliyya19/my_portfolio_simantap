  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-8">
            <h4 class="m-0 text-dark"><?=$title . ' '; if($myArea != null){echo $myArea;} ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-4">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('user/pemantauan'); ?>"><?=$title ?></a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <form action="" method="post">
                  <div class="row">
                    <span class="h3">Pemantauan Bulan <?php if($bulan != ''){echo $bulan .' '. $tahun;} else {echo date('F') .' '.date('Y');} ?></span>                                  
                    <div class="col"></div>                  
                    <div class="col-2">
                      <select name="bulan" id="bulan" class="form-control">
                        <option value="1" <?php if (date("m") == 1) {echo " selected";} ?>>Januari</option>
                        <option value="2" <?php if (date("m") == 2) {echo " selected";} ?>>Februari</option>
                        <option value="3" <?php if (date("m") == 3) {echo " selected";} ?>>Maret</option>
                        <option value="4" <?php if (date("m") == 4) {echo " selected";} ?>>April</option>
                        <option value="5" <?php if (date("m") == 5) {echo " selected";} ?>>Mei</option>
                        <option value="6" <?php if (date("m") == 6) {echo " selected";} ?>>Juni</option>
                        <option value="7" <?php if (date("m") == 7) {echo " selected";} ?>>Juli</option>
                        <option value="8" <?php if (date("m") == 8) {echo " selected";} ?>>Agustus</option>
                        <option value="9" <?php if (date("m") == 9) {echo " selected";} ?>>September</option>
                        <option value="10" <?php if (date("m") == 10) {echo " selected";} ?>>Oktober</option>
                        <option value="11" <?php if (date("m") == 11) {echo " selected";} ?>>November</option>
                        <option value="12" <?php if (date("m") == 12) {echo " selected";} ?>>Desember</option>
                      </select>
                    </div>
                    <div class="col-lg-1">                  
                      <select name="tahun" id="tahun" class="form-control">
                        <?php for ($i=2020; $i < 2030; $i++) { ?>
                          <option value="<?=$i; ?>" <?php if (date("Y") == $i) {echo " selected";} ?>><?=$i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-lg-1">
                      <button type="submit" class="btn btn-success" style="width: 100%;">Show</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-hover">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>AREA</th>
                    <th>PENILAIAN</th>
                    <th>EVIDENCE</th>
                    <th>STATUS</th>
                    <th>TGL UPLOAD</th>
                    <th>TGL UPDATE</th>
                    <th>OPSI</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>NO</th>
                    <th>AREA</th>
                    <th>PENILAIAN</th>
                    <th>EVIDENCE</th>
                    <th>STATUS</th>
                    <th>TGL UPLOAD</th>
                    <th>TGL UPDATE</th>
                    <th>OPSI</th>
                  </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; foreach ($penilaian as $p):?>
                    <tr>
                      <td scope="col" style="width: 1rem;"><?=$i; ?></td>
                      <td scope="col" style="width: 2rem;"><?=$p['area']; ?></td>
                      <td scope="col" style="width: 1rem;">
                        <?=$p['nama']."<br>"; ?>                        
                      </td>
                      <td>
                        <div style="width: 4rem; overflow-x: hidden;">                        
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 50rem;">
                              - <a href="#" class="showdoc" data-toggle="modal" data-target="#viewdoc" data-id="<?=$e['id']; ?>" data-docname="<?=$e['file']; ?>"><?= $e['file'].'<br>';?></a>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 13rem;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>                            
                            <div class="card my-2" style="width: 13rem;">
                            <?php if($e['status'] == "Pending"){ ?>                              
                                <div class="card-body text-primary">
                            <?php }else if($e['status'] == "Revisi"){ ?>
                                <div class="card-body text-danger">
                            <?php }else{ ?>
                                <div class="card-body text-success">
                            <?php } ?>
                            <?php if($user['role'] == 1 || $user['role'] == 5): ?>
                              <a class="float-right close" aria-label="Close" href="<?= base_url().'pemantauan/hapusEvidence/'.$thisArea['id'].'/'.$e['id']; ?>" onclick="return confirm('Hapus dokumen <?=$e['file']; ?> ?');"><span aria-hidden="true">&times;</span>
                              </a>
                            <?php endif; ?>                                  
                                  <p class="card-title"><b><?= $e['status'];?></b></p>
                                  <p class="card-text"><?=$e['keterangan']; ?></p>
                                </div>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>                      
                      <td>
                        <div style="width: 5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_uploaded'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 4.5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $p['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_updated'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td scope="col">
                      <?php foreach ($evidence as $e): ?>
                        <?php if ($e['penilaian'] == $p['id']):?>
                            <?php if($e['status'] == 'Revisi'){ ?>
                              <a href="#" class="add_evidence" data-toggle="modal" data-target="#modal-evidence" data-id="<?=$p['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-danger" style="width: 100%;">Update</span></a><br>
                            <?php }else if($e['status'] == 'Pending'){?>
                              <a href="#" class="add_evidence" data-id="<?=$p['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-primary" style="width: 100%;">Update</span></a><br>
                            <?php }else{ ?>
                              <a href="#" class="add_evidence" data-id="<?=$p['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-success" style="width: 100%;">Update</span></a><br>
                            <?php } ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                      <a href="#" class="add_evidence" data-toggle="modal" data-target="#modal-evidence" data-control="true" data-id="<?=$p['id']; ?>" data-evidence="" data-status=""><span class="btn btn-xs btn-outline-info" style="width: 100%;">+ Evidence</span></a>
                      </td>                      
                    </tr>                    
                  <?php $i++; endforeach; ?>
                  <?php foreach ($tambahan_penilaian as $tp):?>
                    <tr>
                      <td scope="col" style="width: 1rem;"><?=$i; ?></td>
                      <td scope="col" style="width: 2rem;"><?=$tp['area']; ?></td>
                      <td scope="col" style="width: 1rem;">
                        <?=$tp['nama']."<br>"; ?>                        
                      </td>
                      <td>
                        <div style="width: 4rem; overflow-x: hidden;">                        
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $tp['id']):?>
                            <div style="width: 50rem;">
                              - <a href="#" class="showdoc" data-toggle="modal" data-target="#viewdoc" data-id="<?=$e['id']; ?>" data-docname="<?=$e['file']; ?>"><?= $e['file'].'<br>';?></a>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 13rem;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $tp['id']):?>                            
                            <div class="card my-2" style="width: 13rem;">
                            <?php if($e['status'] == "Pending"){ ?>                              
                                <div class="card-body text-primary">
                            <?php }else if($e['status'] == "Revisi"){ ?>
                                <div class="card-body text-danger">
                            <?php }else{ ?>
                                <div class="card-body text-success">
                            <?php } ?>                                  
                                  <p class="card-title"><b><?= $e['status'];?></b></p>
                                  <p class="card-text"><?=$e['keterangan']; ?></p>
                                </div>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>                      
                      <td>
                        <div style="width: 5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $tp['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_uploaded'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td>
                        <div style="width: 4.5rem; overflow-x: hidden;">
                        <?php foreach ($evidence as $e): ?>
                          <?php if ($e['penilaian'] == $tp['id']):?>
                            <div style="width: 10rem;">
                              <?= $e['date_updated'];?>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        </div>
                      </td>
                      <td scope="col">
                      <?php foreach ($evidence as $e): ?>
                        <?php if ($e['penilaian'] == $tp['id']):?>
                            <?php if($e['status'] == 'Revisi'){ ?>
                              <a href="#" class="add_evidence" data-toggle="modal" data-target="#modal-evidence" data-id="<?=$tp['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-danger" style="width: 100%;">Update</span></a><br>
                            <?php }else if($e['status'] == 'Pending'){?>
                              <a href="#" class="add_evidence" data-id="<?=$tp['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-primary" style="width: 100%;">Update</span></a><br>
                            <?php }else{ ?>
                              <a href="#" class="add_evidence" data-id="<?=$tp['id']; ?>" data-evidence="<?= $e['id'];?>" data-status="<?= $e['status'];?>" data-evname="<?= $e['file'];?>"><span class="btn btn-xs btn-outline-success" style="width: 100%;">Update</span></a><br>
                            <?php } ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                      <a href="#" class="add_evidence" data-toggle="modal" data-target="#modal-evidence" data-control="true" data-id="<?=$tp['id']; ?>" data-evidence="" data-status=""><span class="btn btn-xs btn-outline-info" style="width: 100%;">+ Evidence</span></a>
                      </td>                      
                    </tr>                    
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
            </div>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </div>
  <!-- /.content-wrapper-->
        <!-- MODAL EVIDENCE -->
        <div class="modal fade" id="modal-evidence">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Upload Evidence</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body modal-evidence">
                <div>
                  <?php echo form_open_multipart('pemantauan/tambahEvidence')?>
                  <?php if (isset($iscontrol)) {?>
                    <input type="hidden" name="control" id="iscontrol" value="true">
                    <input type="hidden" name="area" id="iscontrol" value="<?=$thisArea['id'];?>">
                  <?php }else{ ?>
                    <input type="hidden" name="control" id="iscontrol" value="false">
                  <?php } ?>
                    <input type="hidden" name="penilaian" id="penilaian_doc">
                    <input type="hidden" name="evidence_id" id="currentevidence">
                    <input type="hidden" name="old_evidence" id="old_evidence">
                    <div class="row">
                      <div class="col-3 tanggal_label">Tanggal</div>
                      <div class="col-9"><input type="date" name="tanggal_upload" id="tanggal_upload" class="form-control" value="<?=date("Y-m-d"); ?>"></div>
                    </div>                    
                    <div class="row mt-2">
                      <div class="col-3">Dokumen</div>
                      <div class="col-9">
                        <div class="card m-0">
                          <label class="custom-file-label">Choose file. . .</label>
                          <input type="file" id="file-evidence" name="evidence" class="custom-file-input">
                        </div>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-3">Keterangan</div>
                      <div class="col-9"><textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea></div>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success float-right ml-2 tbhpegawaibtn">Simpan</button>
                    <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Batal</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- MODAL PREVIEW EVIDENCE -->
        <div class="modal fade" id="viewdoc">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Preview Dokumen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <iframe style="width:100%; height:700px;" frameborder="0" src=""></iframe>  
            </div>
          </div>
        </div>
    <!-- /.content -->    
  </div>

