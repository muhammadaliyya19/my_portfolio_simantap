  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"><?=$title; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('referensi/pengguna'); ?>"><?=$title ?></a></li>
              <li><a href="#" class="btn btn-sm btn-primary ml-2 add_pengguna" data-toggle="modal" data-target="#modal-pengguna">+ Pengguna</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <table id="example1" class="table">
                  <thead>
                  <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>USERNAME</th>
                    <th>EMAIL</th>
                    <th>ROLE</th>
                    <th>HAK AKSES</th>
                    <th>BLOKIR</th>
                    <th>OPSI</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th>NO</th>
                    <th>NAMA</th>
                    <th>USERNAME</th>
                    <th>EMAIL</th>
                    <th>ROLE</th>
                    <th>HAK AKSES</th>
                    <th>BLOKIR</th>
                    <th>OPSI</th>
                  </tr>
                  </tfoot>
                  <tbody>
                  <?php $i = 1; foreach ($pengguna as $p):?>
                    <tr>
                      <td><?=$i; ?></td>
                      <td><?=$p['nama']; ?></td>
                      <td><?=$p['username']; ?></td>
                      <td><?=$p['email']; ?></td>
                      <td><?=$p['role']; ?></td>
                      <td><?=$p['nama_jabatan']; ?></td>
                      <td><?=$p['blokir']; ?></td>
                      <td>
                        <a href="#" class="edit_pengguna" data-toggle="modal" data-target="#modal-pengguna" data-id="<?=$p['id']; ?>"><span class="btn btn-xs btn-outline-success"><i class="fas fa-pencil-alt"></i></span></a>
                        <a href="<?=base_url('referensi/hapusPengguna/'.$p['id']); ?>" onclick="return confirm('Hapus pengguna?')"><span class="btn btn-xs btn-outline-danger"><i class="fas fa-trash"></i></span></a>
                      </td>
                    </tr>
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- MODAL PENGGUNA -->
  <div class="modal fade" id="modal-pengguna">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Pengguna</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body modal-pengguna">
                <form action="<?= base_url('referensi/tambahpengguna'); ?>" method="post">
                  <input type="hidden" id="id" class="form-control" name="id">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Pegawai
                      </div>
                      <div class="col-10">
                        <select name="nip" id="nip" class="form-control">
                          <option value="">Pilih Pegawai</option>
                          <?php foreach ($pegawai as $p): ?>
                            <option value="<?=$p['NIP'];?>"><?=$p['nama']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Username
                      </div>
                      <div class="col-10">
                        <input type="text" id="username" class="form-control" placeholder="Username . . . " name="username">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">                  
                    <div class="row">
                      <div class="col-2">
                        Password
                      </div>
                      <div class="col-10">
                        <input type="password" id="password" class="form-control" placeholder="Password . . . " name="password">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        Ulang Password
                      </div>
                      <div class="col-10">
                        <input type="password" id="retypepwd" class="form-control" placeholder="Retype password . . . " name="repassword">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-2">
                        E-mail
                      </div>
                      <div class="col-10">
                        <input type="email" id="email" class="form-control" placeholder="Email . . . " name="email">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">                  
                    <div class="row">
                      <div class="col-2">
                        Role
                      </div>
                      <div class="col-10" >
                        <select name="role" id="role" class="form-control">
                          <option value="">Pilih Role User</option>
                          <?php foreach ($role as $r):?>
                            <option value="<?=$r['id']; ?>"><?=$r['nama']; ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>                      
                    </div>
                  </div>
                  <div class="form-group">                  
                    <div class="row">
                      <div class="col-2">
                        Blokir Pengguna
                      </div>
                      <div class="col-10" >
                        <select name="blokir" id="blokir" class="form-control">
                          <option value="">Pilih Status Blokir</option>
                          <option value="ya">Ya</option>
                          <option value="tidak">Tidak</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success float-right ml-2">Simpan</button>
                  <button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Batal</button>
                </form>
            </div>
          </div>            
        </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
