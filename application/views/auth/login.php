<div id="large-header" style="position: absolute; padding: 0px; margin: 0px;">
  <canvas id="demo-canvas" style="margin: 0; padding: 0; z-index: -100; background: #003a1e;"></canvas>
    <script src="<?= base_url('assets/login/')?>TweenLite.min.js.download"></script>
    <script src="<?= base_url('assets/login/')?>EasePack.min.js.download"></script>
    <script src="<?= base_url('assets/login/')?>demo-1.js.download"></script>
</div>
<div class="row" align="center">
  <div class="col">
    <div>
      <img src="<?=base_url('assets/img/fav/logo.png'); ?>" style="height: 125px; align-self: center;" class="mb-2"><br>
      <a href="<?=base_url(); ?>" style="color: white; font-size: 35px;"><b>SIMANTAP</b></a><br>
      <a href="<?=base_url(); ?>" style="color: white; font-size: 20px;"><b>Sistem Informasi Monitoring & Evaluasi Akreditasi <br>Pengadilan Tinggi Banten</b></a>
    </div>
  </div>
</div>
<div class="row" align="center">
  <div class="col">        
    <div class="login-box mt-2" style="width: 450px;">
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body">
          <form action="<?=base_url('auth'); ?>" method="post">
            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Username" name="username">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Password" name="password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <button type="submit" class="btn btn-success btn-block">LOGIN</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
          <div class="text-center p-2">
            <a href="<?=base_url(); ?>">&copy Pengadilan Tinggi Banten</a><br>
          </div>
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
    <!-- /.login-box -->
  </div>
</div>
