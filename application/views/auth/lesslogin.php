<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="<?=base_url('assets/img/fav/logo.png'); ?>">
	<title>SIMANTAP</title>
	<link rel="stylesheet/less" type="text/css" href="<?=base_url('assets/less/'); ?>styles.less" />
	<script>
	  less = {
	    env: "development",
	    async: false,
	    fileAsync: false,
	    poll: 1000,
	    functions: {},
	    dumpLineNumbers: "comments",
	    relativeUrls: false,
	    rootpath: ":/a.com/"
	  };
	</script>
	<script src="<?=base_url('assets/js/'); ?>less.js" type="text/javascript"></script>
</head>
<body>
<div class="wrapper">
	<div class="container">
		<h1>Welcome</h1>
		
		<form class="form">
			<input type="text" placeholder="Username">
			<input type="password" placeholder="Password">
			<button type="submit" id="login-button">Login</button>
		</form>
	</div>
	
	<ul class="bg-bubbles">
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
		<li></li>
	</ul>
</div>
<!-- jQuery -->
<script src="<?=base_url('assets/adminlte/'); ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('assets/adminlte/'); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
	$("#login-button").click(function(event){
		event.preventDefault();
		$('form').fadeOut(500);
		$('.wrapper').addClass('form-success');
	});
</script>
</body>
</html>