  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark">Area <?=$namearea['nama']; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="">Dashboard</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">        
        <!--  -->

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header m-0">
                <form action="" method="post">                    
                  <div class="row">
                    <div class="col-10">
                      <h4  align="center">Kinerja Berdasarkan Kepatuhan Pengunggahan Evidence Tahun <?=$tahun; ?></h4>
                    </div>
                    <div class="col">
                        <select name="tahun" id="tahun" class="form-control">
                          <?php for ($i=2020; $i < 2030; $i++) { ?>
                            <option value="<?=$i; ?>" <?php if (date("Y") == $i) {echo " selected";} ?>><?=$i; ?></option>
                          <?php } ?>
                        </select>
                    </div>
                    <div class="col">
                      <button type="submit" class="btn btn-success" style="width: 100%;">Show</button>
                    </div>
                  </div>
                </form>
              </div>
              <div class="card-body">
                <table id="example1" class="table">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Area</th>
                    <th>
                      <div class="row">
                        <div class="col my-1" align="center">Bulan</div>
                      </div>
                      <div class="row">
                        <div class="col">Jan</div>
                        <div class="col">Feb</div>
                        <div class="col">Mar</div>
                        <div class="col">Apr</div>
                        <div class="col">Mei</div>
                        <div class="col">Jun</div>
                        <div class="col">Jul</div>
                        <div class="col">Ags</div>
                        <div class="col">Sep</div>
                        <div class="col">Okt</div>
                        <div class="col">Nov</div>
                        <div class="col">Des</div>
                      </div>
                    </th>                    
                  </tr>
                  </thead>                 
                  <tbody>
                  <?php $i = 0; foreach ($area as $a):?>
                    <tr>
                      <td><?=$i+1; ?></td>
                      <td><?=$a['nama']; ?></td>
                      <td>
                        <div class="row"  align="center">
                          <div class="col">
                            <?php if ($prosentase[$i][1] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][1]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][2] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>                              
                            <?php } ?>
                            <?=$prosentase[$i][2]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][3] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][3]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][4] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][4]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][5] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][5]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][6] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][6]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][7] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][7]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][8] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][8]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][9] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][9]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][10] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][10]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][11] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][11]; ?> %
                          </div>
                          <div class="col">
                            <?php if ($prosentase[$i][12] != 100) {?>
                              <i class="fa fa-exclamation" style="color:red"></i><br>                              
                            <?php }else{ ?>
                              <i class="fa fa-check" style="color:green"></i><br>
                            <?php } ?>
                            <?=$prosentase[$i][12]; ?> %
                          </div>
                        </div>
                      </td>                      
                    </tr>
                  <?php $i++; endforeach; ?>
                  </tbody>                  
                </table>
              </div>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper