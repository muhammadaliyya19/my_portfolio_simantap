  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"><?=$title; ?></h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=base_url(); ?>">SIMANTAP</a></li>
              <li class="breadcrumb-item active"><a href="<?=base_url('user/profile'); ?>"><?=$title ?></a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <?php echo $this->session->flashdata('message'); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-4">
            <div class="card card-success card-outline" style="height: 100%;">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?=base_url(); ?>assets/img/fav/Network-Profile.png"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?=$user['nama']; ?></h3>

                <p class="text-muted text-center"><?=$user['jabatan']; ?></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>NIP</b> <a class="float-right"><?=$user['nip_pegawai']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Last Updated</b> <a class="float-right"><?=$user['date_updated']; ?></a>
                  </li>
                  <li class="list-group-item">
                    <b>Join Date</b> <a class="float-right"><?=$user['date_created']; ?></a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <div class="col-8">
            <div class="card" style="height: 100%;">
              <div class="card-header">
                Ubah Profil Anda
              </div>
              <div class="card-body">
                <form action="<?=base_url('user/profile'); ?>" method="post">
                  <div class="row mt-2">
                    <div class="col-3">Username</div>
                    <div class="col">
                      <input type="hidden" name="id_user" class="form-control" value="<?=$user['id']; ?>">
                      <input type="text" name="username" class="form-control" placeholder="Username" value="<?=$user['username']; ?>">
                    </div>
                  </div>
                  <div class="row mt-2">
                    <div class="col-3">E-mail</div>
                    <div class="col">
                      <input type="email" name="email" class="form-control" placeholder="E-mail" value="<?=$user['email']; ?>">
                    </div>
                  </div>
                  <div class="row mt-2">
                    <div class="col-3">Password Lama</div>
                    <div class="col">
                      <input type="password" name="old_password" class="form-control" placeholder="Masukkan password ..." >
                    </div>
                  </div>
                  <div class="row mt-2">                
                    <div class="col-3">Password Baru</div>
                    <div class="col">
                      <input type="password" name="password" class="form-control" placeholder="Password baru ...">
                    </div>
                    <div class="col">
                      <input type="password" name="repassword" class="form-control" placeholder="Konfirmasi password ...">
                    </div>
                  </div>
                  <div class="row mt-2">
                    <div class="col">
                      <button type="submit" class="btn btn-success float-right ml-2">Simpan</button>
                      <a href="#" class="btn btn-secondary float-right">Batal</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper