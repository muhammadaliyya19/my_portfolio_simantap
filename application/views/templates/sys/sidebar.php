  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
      <a href="<?=base_url(); ?>" class="brand-link" align="center">
        <img src="<?=base_url('assets/img/fav/logos.png'); ?>" alt="S Logo" class="brand-image img-circle">
        <span class="brand-text font-weight-light"><img src="<?=base_url('assets/img/fav/logo1.png'); ?>" style="height: 35px; margin-left: -50px; margin-top: -15px;"></span>
      </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image mt-2 mr-1">
          <img src="<?=base_url('assets/img/fav/Network-Profile.png'); ?>" alt="profile_image" class="img-circle">
        </div>
        <div class="info">
          <a href="#" class="d-block"><b><?=$user['nama']  . '</b><br>' . $user['nama_role'];?></a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->          
          <li class="nav-item">
            <a href="<?=base_url(); ?>" class="nav-link <?php if($title=='Simantap'){echo('active');} ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>                    
          <?php if($user['role'] != '6'): ?> 
            <?php if($user['role'] == '1'): ?>          
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link <?php if($title=='Pegawai' || $title=='Pengguna'){echo('active');} ?>">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                    Referensi
                    <i class="right fas fa-angle-down"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?=base_url('referensi/pegawai'); ?>" class="nav-link <?php if($title=='Pegawai'){echo('active');} ?>">
                      <i class="nav-icon far fa-circle"></i>
                      <p>Pegawai</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?=base_url('referensi/pengguna'); ?>" class="nav-link <?php if($title=='Pengguna'){echo('active');} ?>">
                      <i class="nav-icon far fa-circle"></i>
                      <p>Pengguna</p>                  
                    </a>
                  </li>
                </ul>
              </li>
            <?php endif; ?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if($title=='Validasi' || $title=='Validasi Evidence'){echo('active');} ?>">
              <i class="nav-icon fas fa-desktop"></i>
              <p>
                Validasi
                <i class="right fas fa-angle-down"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?=base_url('pemantauan/validasi_all'); ?>" class="nav-link">
                    <i class="nav-icon far fa-circle"></i>
                    <p>Keseluruhan</p>
                  </a>
                </li>
              <?php foreach($area as $a): ?>
                <li class="nav-item">
                  <a href="<?=base_url('pemantauan/validasi/'. $a['id']); ?>" class="nav-link">
                    <i class="nav-icon far fa-circle"></i>
                    <p><?=$a['nama']?></p>
                  </a>
                </li>
              <?php endforeach; ?>              
            </ul>
          </li>                  
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link <?php if($title=='Pemantauan'){echo('active');} ?>">
              <i class="nav-icon fas fa-check"></i>
              <p>
                Pemantauan
                <i class="right fas fa-angle-down"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">                
              <?php foreach($area as $a): ?>
                <li class="nav-item">
                  <a href="<?=base_url('pemantauan/control/'. $a['id']); ?>" class="nav-link">
                    <i class="nav-icon far fa-circle"></i>
                    <p><?=$a['nama']?></p>
                  </a>
                </li>
              <?php endforeach; ?>              
            </ul>
          </li>                  
          <li class="nav-item">
            <a href="<?=base_url('laporan'); ?>" class="nav-link <?php if($title=='Laporan'){echo('active');} ?>">
              <i class="nav-icon fas fa-file"></i>
              <p>Laporan</p>
            </a>
          </li>
            <li class="nav-item">
              <a href="<?=base_url('user/profile'); ?>" class="nav-link <?php if($title=='Profil Pengguna'){echo('active');} ?>">
                <i class="nav-icon fas fa-user"></i>
                <p>Profil Pengguna</p>
              </a>
            </li>         
          <?php endif; ?>
          <?php if($user['role'] == '6'): ?>
          <li class="nav-item">
            <a href="<?=base_url('pemantauan'); ?>" class="nav-link <?php if($title=='Pemantauan'){echo('active');} ?>">
              <i class="nav-icon fas fa-desktop"></i>
              <p>Pemantauan</p>
            </a>
          </li>
          <li class="nav-item">
              <a href="<?=base_url('user/profile'); ?>" class="nav-link <?php if($title=='Profil Pengguna'){echo('active');} ?>">
                <i class="nav-icon fas fa-user"></i>
                <p>Profil Pengguna</p>
              </a>
            </li>  
          <?php endif; ?>
          <li class="nav-item">
            <a href="<?=base_url('auth/logout'); ?>" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>