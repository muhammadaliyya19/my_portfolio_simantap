<!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-success navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>      
      <li class="nav-item">
        <span class="nav-link"><strong><?=$navtitle; ?></strong></span>
      </li>
    </ul>       
  </nav>
  <!-- /.navbar -->