<?php 
	class Pegawai_model extends CI_Model
	{
		
		public function getAllPegawai()
		{
			$queryPegawai = "SELECT pegawai.id, pegawai.NIP, pegawai.nama, pegawai.golongan, pegawai.pangkat, pegawai.ruang, area.nama as area, jabatan.nama_jabatan, pegawai.aktif FROM pegawai INNER JOIN jabatan ON pegawai.jabatan = jabatan.id INNER JOIN area on pegawai.area = area.id";
      		$query = $this->db->query($queryPegawai)->result_array();
			return $query;
		}

		public function tambahDataPegawai()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(				
				"NIP"			=> $this->input->post('nip', true),
				"nama"			=> $this->input->post('nama', true),
				"golongan"		=> $this->input->post('golongan', true),
				"pangkat"		=> $this->input->post('pangkat', true),
				"area"			=> $this->input->post('area', true),
				"ruang"			=> $this->input->post('ruang', true),
				"jabatan"		=> $this->input->post('jabatan', true),
				"alamat"		=> $this->input->post('alamat', true),
				"aktif"			=> $this->input->post('keaktifan', true),
				"date_updated" 	=> $now->format('Y-m-d H:i:s'),
				"date_created" 	=> $now->format('Y-m-d H:i:s')
			);
			$this->db->insert('pegawai', $data);
			return 1;
		}

		public function hapusDataPegawai($id)
		{
			$this->db->delete('pegawai', ['id' => $id]);
			return 1;
		}

		public function editDataPegawai()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(
				"NIP"			=> $this->input->post('nip', true),
				"nama"			=> $this->input->post('nama', true),
				"golongan"		=> $this->input->post('golongan', true),
				"pangkat"		=> $this->input->post('pangkat', true),
				"area"			=> $this->input->post('area', true),
				"ruang"			=> $this->input->post('ruang', true),
				"jabatan"		=> $this->input->post('jabatan', true),
				"alamat"		=> $this->input->post('alamat', true),
				"aktif"			=> $this->input->post('keaktifan', true),
				"date_updated" 	=> $now->format('Y-m-d H:i:s')
			);
			$this->db->where('id', $this->input->post('id'));
			if($this->db->update('pegawai', $data)){
				return 1;
			}else{
				return 0;
			}
		}

		public function getPegawaiById($id)
		{
			return $this->db->get_where('pegawai' , ['id' => $id])->row_array();
		}

		public function getPegawaiByNIP($nip)
		{
			return $this->db->get_where('pegawai' , ['nip' => $nip])->row_array();
		}

		//AREA PENGGUNA
		public function getAllArea()
		{
			return $query = $this->db->get('area')->result_array();
		}

		public function getAreaById($id)
		{
			return $query = $this->db->get_where('area', ['id' => $id])->row_array();
		}

		// JABATAN PEGAWAI
		public function getAllJabatan()
		{
			return $query = $this->db->get('jabatan')->result_array();
		}

		public function tambahDataJabatan()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(				
				"NIP"			=> $this->input->post('nip', true),
				"nama"			=> $this->input->post('nama', true),
				"golongan"		=> $this->input->post('golongan', true),
				"pangkat"		=> $this->input->post('pangkat', true),
				"jabatan"		=> $this->input->post('jabatan', true),
				"alamat"		=> $this->input->post('alamat', true),
				"aktif"			=> $this->input->post('keaktifan', true),
				"date_updated" 	=> $now->format('Y-m-d H:i:s'),
				"date_created" 	=> $now->format('Y-m-d H:i:s')
			);
			$this->db->insert('pegawai', $data);
			return 1;
		}

		public function hapusDataJabatan($id)
		{
			$this->db->delete('pegawai', ['id' => $id]);
			return 1;
		}

		public function getJabatanById($id)
		{
			return $this->db->get_where('jabatan' , ['id' => $id])->row_array();
		}

		public function getJabatan($id)
		{
			echo json_encode($this->db->get_where('jabatan' , ['id' => $id])->row_array());
		}

	}
?>