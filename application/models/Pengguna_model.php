<?php 
	class Pengguna_model extends CI_Model
	{
		
		public function getAllPengguna()
		{
			$queryPengguna = "SELECT user.id, pegawai.nama, user.username, user.email, jabatan.nama_jabatan, user.blokir, role.nama as role FROM user 
			INNER JOIN pegawai ON user.nip_pegawai = pegawai.NIP INNER JOIN jabatan ON pegawai.jabatan = jabatan.id
			INNER JOIN role ON user.role = role.id";
      		$query = $this->db->query($queryPengguna)->result_array();
			return $query;
		}

		public function tambahDataPengguna()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			if ($this->input->post('is_validator', true) == null) {
				$data = array(				
					"nip_pegawai"	=> $this->input->post('nip', true),
					"username"		=> $this->input->post('username', true),
					"password"		=> $this->input->post('password', true),
					"email"			=> $this->input->post('email', true),
					"role"			=> $this->input->post('role', true),
					"blokir"		=> $this->input->post('blokir', true),
					"date_updated" 	=> $now->format('Y-m-d H:i:s'),
					"date_created" 	=> $now->format('Y-m-d H:i:s')
				);
			}else{
				$data = array(				
					"nip_pegawai"	=> $this->input->post('nip', true),
					"username"		=> $this->input->post('username', true),
					"password"		=> $this->input->post('password', true),
					"email"			=> $this->input->post('email', true),
					"role"			=> $this->input->post('role', true),
					"blokir"		=> $this->input->post('blokir', true),
					"date_updated" 	=> $now->format('Y-m-d H:i:s'),
					"date_created" 	=> $now->format('Y-m-d H:i:s')
				);
			}
			$this->db->insert('user', $data);
			return 1;
		}

		public function hapusDataPengguna($id)
		{
			$this->db->delete('user', ['id' => $id]);
			return 1;
		}

		public function editDataPengguna()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(				
				"nip_pegawai"	=> $this->input->post('nip', true),
				"username"		=> $this->input->post('username', true),
				"password"		=> $this->input->post('password', true),
				"email"			=> $this->input->post('email', true),
				"role"			=> $this->input->post('role', true),
				"blokir"		=> $this->input->post('blokir', true),
				"date_updated" 	=> $now->format('Y-m-d H:i:s')
			);
			$this->db->where('id', $this->input->post('id'));
			$this->db->update('user', $data);
			return 1;
		}

		public function updateProfile()
		{
			$thisUser = $this->getPenggunaById($this->input->post('id_user', true));
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			if ($this->input->post('password', true) == $this->input->post('repassword', true) && $this->input->post('old_password', true) == $thisUser['password']) {
				$data = array(				
					"username"		=> $this->input->post('username', true),
					"password"		=> $this->input->post('password', true),
					"email"			=> $this->input->post('email', true),
					"date_updated" 	=> $now->format('Y-m-d H:i:s')
				);
				$this->db->where('id', $this->input->post('id_user'));
				$this->db->update('user', $data);
				return 1;
			}else{
				return 0;
			}
		}

		public function getPenggunaById($id)
		{
			return $this->db->get_where('user' , ['id' => $id])->row_array();
		}

		public function getPenggunaByUsername($username)
		{
			return $this->db->get_where('user' , ['username' => $username])->row_array();
		}

		// ROLE PENGGUNA

		public function getAllRole()
		{
			return $this->db->get('role')->result_array();			
		}

		public function getRoleById($id)
		{
			return $this->db->get_where('role' , ['id' => $id])->row_array();
		}
	}
?>