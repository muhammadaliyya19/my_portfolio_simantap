<?php 
	class Pemantauan_model extends CI_Model
	{
		// FUNGSI - FUNGSI UNTUK MENGAMBIL POIN PENILAIAN DAN BUTIR PENILAIAN
		public function getAllButirPenilaian(){
			$queryButirPenilaian = "SELECT * FROM butir_evidence";
      		$query = $this->db->query($queryButirPenilaian)->result_array();
			return $query;	
		}

		public function getAllPenilaian()
		{
			$queryPenilaian = "SELECT penilaian.id, area.nama as area, penilaian.nama FROM penilaian INNER JOIN area ON penilaian.area = area.id";
      		$query = $this->db->query($queryPenilaian)->result_array();
			return $query;
		}

		public function getPenilaianByArea($id)
		{
			$queryPenilaian = "SELECT penilaian.id, area.nama as area, penilaian.nama FROM penilaian INNER JOIN area ON penilaian.area = area.id WHERE area.id = $id";
      		$query = $this->db->query($queryPenilaian)->result_array();
			return $query;
		}

		public function getTambahanPenilaian($areatugas){
			$queryPenilaian = "SELECT penilaian.id, area.nama as area, penilaian.nama FROM penilaian INNER JOIN area ON penilaian.area = area.id WHERE penilaian.tugas = $areatugas";
      		$query = $this->db->query($queryPenilaian)->result_array();
			return $query;
		}
		
		// FUNGSI UNTUK EVIDENCE
		public function getAllEvidence(){
			return $this->db->get('evidence')->result_array();
		}

		public function getEvidenceByMonthYear(){
			$query_bydate = "SELECT * FROM evidence WHERE evidence.date_uploaded LIKE '%".$_POST['my']."%'";
			$query = $this->db->query($query_bydate)->result_array();
			return $query;
		}

		public function getEvidenceByArea($area){
			$queryEvidence = "SELECT evidence.id, evidence.status, area.nama as area, penilaian.nama as penilaian, 
							  evidence.file, evidence.date_uploaded, evidence.date_updated,  user.username FROM area 
							  INNER JOIN penilaian ON penilaian.area = area.id 
							  INNER JOIN evidence ON penilaian.id = evidence.penilaian 
							  INNER JOIN user ON evidence.upload_by = user.id 
							  WHERE area.id = $area";
      		$query = $this->db->query($queryEvidence)->result_array();
			return $query;
		}

		public function getEvidenceByMonth($month, $year){
			$queryEvidence = "SELECT * FROM evidence WHERE MONTH(evidence.date_uploaded) = $month AND YEAR(evidence.date_uploaded) = $year";
      		$query = $this->db->query($queryEvidence)->result_array();
			return $query;
		}

		// MENCATAT DATA EVIDENCE KE DATABASE
		public function tambahDataEvidence($data)
		{
			$data = array(				
				"file"			=> $data['file'],
				"penilaian"		=> $data['penilaian'],
				"upload_by"		=> $data['upload_by'],
				"keterangan"	=> $data['keterangan'],
				"status"		=> $data['status'],
				"date_uploaded"	=> $data['date_uploaded']
			);
			$this->db->insert('evidence', $data);
			return 1;
		}

		// PROSES UPDATE STATUS EVIDENCE OLEH VALIDATOR / TOP MANAJEMEN
		public function validasi_evidence()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(
				"status" 		=> $this->input->post('status'),
				"keterangan" 	=> $this->input->post('keterangan')
			);
			$this->db->where('id', $this->input->post('evidence_id'));
			$this->db->update('evidence', $data);
			return 1;
		}

		// FUNGSI REVISI EVIDENCE
		// PROSES UPDATE STATUS EVIDENCE OLEH VALIDATOR / TOP MANAJEMEN
		public function revisi_evidence()
		{
			date_default_timezone_set("Asia/Jakarta");
			$now = new DateTime();
			$data = array(
				"file" 			=> $_FILES['evidence']['name'],
				"status" 		=> "Pending",
				"keterangan" 	=> $this->input->post('keterangan')
			);
			$this->db->where('id', $this->input->post('evidence_id'));
			$this->db->update('evidence', $data);
			return 1;
		}

		
		public function getEvidencebyId($id){
			return $this->db->get_where('evidence' , ['id' => $id])->row_array();			
		}

		// FUNGSI HAPUS EVIDENCE
		public function hapus_evidence($id)
		{			
			$this->db->delete('evidence', ['id' => $id]);
			return 1;
		}
		
		// FUNGSI UNTUK KALKULASI PROSENTASE KEPATUHAN DI DASHBOARD ADMIN		
		public function getDashboardProsentase()
		{
			$tahun = 0;
			if ($_POST != null) {
				$tahun = $_POST['tahun'];
			}else{
				$tahun = date('Y');
			}
			$area = $this->db->get('area')->result_array();			
			$prosentase = [];
			foreach ($area as $a) {
				$prosentasearea = [];
				array_push($prosentasearea, $a['nama']);
				for ($j=1; $j < 13; $j++) {
					$p = $this->getSumEvidence($a['id'], $j, $tahun);
					array_push($prosentasearea, round($p,0));
				}
				array_push($prosentase, $prosentasearea);
			}
			return $prosentase;			
		}

		// FUNGSI UNTUK KALKULASI PROSENTASE KEPATUHAN DI DASHBOARD USER	
		public function getUserDashboardProsentase()
		{
			$tahun = 0;
			if ($_POST != null) {
				$tahun = $_POST['tahun'];
			}else{
				$tahun = date('Y');
			}
			$area = $this->db->get_where('area', ['id' => $this->session->userdata('user')['area']])->row_array();			
			$prosentasearea = [];
			array_push($prosentasearea, $area['nama']);
			for ($j=1; $j < 13; $j++) {
				$p = $this->getSumEvidence($area['id'], $j, $tahun);
				array_push($prosentasearea, round($p,0));
			}
			return $prosentasearea;			
		}

		// MENGAMBIL JUMLAH EVIDENCE SUATU BULAN DAN AREA TERTENTU
		public function getSumEvidence($area, $month, $year)
		{
			// echo 'pada area: '.$area."- di Bulan / tahun ".$month."-".$year.'<br>';
			$queryPenilaian = "SELECT penilaian.id, area.nama as area, penilaian.nama FROM penilaian INNER JOIN area ON penilaian.area = area.id WHERE area.id = $area";
      		$penilaian = $this->db->query($queryPenilaian)->result_array();
      		// DIDAPAT JUMLAH PENILAIANNYA di area ini
      		$jum_penilaian = count($penilaian);
      		      		
      		// MENCARI JUMLAH EVIDENCE DI AREA INI DAN DI BULAN TAHUN INI
      		$queryEvidence = "";
      		if ($month < 10) {
      		$queryEvidence = "SELECT evidence.id, evidence.file, evidence.status, evidence.penilaian, penilaian.area FROM evidence INNER JOIN penilaian ON evidence.penilaian = penilaian.id WHERE penilaian.area LIKE $area AND evidence.date_uploaded LIKE '%$year-0$month%'";      		
      		}else{      			
      		$queryEvidence = "SELECT evidence.id, evidence.file, evidence.status, evidence.penilaian, penilaian.area FROM evidence INNER JOIN penilaian ON evidence.penilaian = penilaian.id WHERE penilaian.area LIKE $area AND evidence.date_uploaded LIKE '%$year-$month%'";
      		}
      		$evidence = $this->db->query($queryEvidence)->result_array();
      		$jum_evidenceApproved = 0;
      		foreach ($evidence as $e) {
      			if ($e['status'] == "Diterima") {
      				$jum_evidenceApproved++;
      			}
      		}
      		// echo $jum_evidenceApproved . ' / '. $jum_penilaian . ' = ';
      		$prosentase = ($jum_evidenceApproved / $jum_penilaian) * 100;
      		// MENGEMBALIKAN PROSENTASE JUMLAH EVIDENCE / Total penilaian 
      		return $prosentase;
		}
	}
?>