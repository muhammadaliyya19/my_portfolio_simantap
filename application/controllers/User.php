<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');			
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Pemantauan_model');
		is_logged_in();
	}
		
	public function index()
	{
		$data['title'] = 'Simantap';
		$data['navtitle'] = '1. Dashboard';
		$data['user'] = $this->session->userdata('user');
		$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		$data['sum_penilaian'] = count($this->Pemantauan_model->getPenilaianByArea($data['user']['area']));
		$data['prosentase'] = $this->Pemantauan_model->getUserDashboardProsentase();	
		if ($_POST != null) {
			$data['tahun'] = $this->input->post('tahun', true);
		}else{
			$data['tahun'] = date('Y');
		}	
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('user/index');
		$this->load->view('templates/sys/footer');
	}

	public function profile()
	{
		$data['title'] = 'Profil Pengguna';
		$data['user'] = $this->Pengguna_model->getPenggunaById($this->session->userdata('user')['id']);
		$data['user'] += $this->session->userdata('user');
		if ($data['user']['role'] < 6) {
			$data['navtitle'] = '8. Profil Pengguna';
			if ($this->session->userdata('user')['role'] == 5) {
				$data['navtitle'] = '5. Profil Pengguna';
			}
			$data['area'] = $this->Pegawai_model->getAllArea();
		}else{
			$data['navtitle'] = '3. Profil Pengguna';
			$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		}
		$this->form_validation->set_rules('id_user', 'Id User', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		$this->form_validation->set_rules('old_password', 'Password Lama', 'required');
		$this->form_validation->set_rules('password', 'Password Baru', 'required');
		$this->form_validation->set_rules('repassword', 'Ulang Password Baru', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('templates/sys/header', $data);
			$this->load->view('templates/sys/navbar');
			$this->load->view('templates/sys/sidebar');
			$this->load->view('user/profile');
			$this->load->view('templates/sys/footer');
		}else{
			$result = $this->Pengguna_model->updateProfile();
			if ($result > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Profil berhasil diubah!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
				
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Profil gagal diubah! Mohon ulang password anda !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			}
			redirect('user/profile');
		}
	}	
}