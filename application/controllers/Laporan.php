<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';
use PhpOffice\PhpWord\PhpWord;
class Laporan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		//session_start();
		$this->load->library('form_validation');			
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Pemantauan_model');
		is_logged_in();
	}
		
	public function index()
	{
		$data['title'] = 'Laporan';
		$data['navtitle'] = '7. Laporan';
		if ($this->session->userdata('user')['role'] == 5) {
			$data['navtitle'] = '4. Laporan';
		}
		$data['user'] = $this->session->userdata('user');
		if ($data['user']['role'] < 6) {
			$data['area'] = $this->Pegawai_model->getAllArea();
		}else{
			$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('laporan/index');
		$this->load->view('templates/sys/footer');
		// echo "Dashboard untuk Admin";
	}

	public function download()
	{
		var_dump($_POST);
		echo date('H:i:s') , ' Creating new TemplateProcessor instance...<br>' ;
		
		// BIKIN TABEL DENGAN XML
		//Create table		
		$document_with_table = new PhpWord();
		$fancyTableStyle = [
		    'borderSize'  => 6,
		    'borderColor' => '000000',
		    'cellMargin'  => 20
		];
		$section = $document_with_table->addSection();
		$table = $section->addTable($fancyTableStyle);		
		// Mengambil prosentase saat ini
		$prosentase = $this->Pemantauan_model->getDashboardProsentase();
		for ($r = 0; $r < count($prosentase); $r++) {
		    if ($r==0) {
		    	$table->addRow();
			    $table->addCell()->addText("Area Kerja");
			    $table->addCell(550)->addText("Jan");
			    $table->addCell(550)->addText("Feb");
			    $table->addCell(550)->addText("Mar");
			    $table->addCell(550)->addText("Apr");
			    $table->addCell(550)->addText("Mei");
			    $table->addCell(550)->addText("Jun");
			    $table->addCell(550)->addText("Jul");
			    $table->addCell(550)->addText("Aug");
			    $table->addCell(550)->addText("Sept");
			    $table->addCell(550)->addText("Okt");
			    $table->addCell(550)->addText("Nov");
			    $table->addCell(550)->addText("Des");
		    }
		    $table->addRow();
		    for ($c = 0; $c < count($prosentase[$r]); $c++) {
		    	if ($c == 0) {
		        	$table->addCell()->addText($prosentase[$r][$c]);		    		
		    	}else{
		        	$table->addCell(550)->addText($prosentase[$r][$c]."%");
		    	}
		    }
		}

		// Create writer to convert document to xml
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($document_with_table, 'Word2007');

		// Get all document xml code
		$fullxml = $objWriter->getWriterPart('Document')->write();

		// Get only table xml code
		$tablexml = preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullxml);

		//Open template with ${table}
		// $template_document = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');

		// Replace mark by xml code of table
		// $template_document->setValue('table', $tablexml);


		// PROCESSING TMEPLATE
		date_default_timezone_set("Asia/Jakarta");
		$now = new DateTime();
		$hari = $this->input->post('hari');
		$tanggal = $this->input->post('tanggal');
		$u_bulan = $this->input->post('bulan');
		$u_tahun = $this->input->post('tahun');
		$catatan = $this->input->post('catatan');
		$time = strtotime($tanggal);				
		// 2003-10-16
		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('assets/docs/Template.docx');
		$templateProcessor->setValue('hari',$hari);
		$templateProcessor->setValue('tanggal',date('d',$time));
		$templateProcessor->setValue('bulan',date('m',$time));
		$templateProcessor->setValue('tahun',date('Y',$time));
		$templateProcessor->setValue('_bulan',date('F',$time));
		$templateProcessor->setValue('bulan_',$u_bulan);
		$templateProcessor->setValue('tahun_',$u_tahun);
		$templateProcessor->setValue('tabel', $tablexml);
		$templateProcessor->setValue('catatan',$catatan);
		echo date('H:i:s'), ' Saving the result document...';
		$file = 'Laporan-'.$now->format('d-m-Y_H-i-s').'.docx';
		$file_path = 'assets/docs/'.$file;
		$templateProcessor->saveAs('assets/docs/'.$file);
		// die;
		echo "<br>File dibuat ".date('H-i-s')."<br>";
		// sleep(4);
		// sleep(5);
		$file_content = file_get_contents('assets/docs/'.$file); // Read the file's contents
		if(file_exists($file_path)){
		    unlink($file_path);
		}
		echo "File dihapus".date('H-i-s')."<br>";
		// flush();
		// $this->deleteLaporan($file);
		$this->load->helper('download');
		// force_download('assets/docs/'.$file, NULL);
		force_download($file, $file_content);
		echo "File didownload ".date('H-i-s')."<br>";

		// var_dump($prosentase);
		// die;
	}

	public function deleteLaporan($file)
	{
		unlink(FCPATH.'assets/docs/'.$file);		
	}
}