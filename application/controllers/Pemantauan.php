<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pemantauan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');			
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Pemantauan_model');
		is_logged_in();
	}
	
	public function index()
	{
		$data['title'] = 'Pemantauan';
		$data['navtitle'] = '2. Pemantauan';
		$data['user'] = $this->session->userdata('user');
		$data['evidence'] = $this->Pemantauan_model->getAllEvidence();
		$data['penilaian'] = $this->Pemantauan_model->getPenilaianByArea($data['user']['area']);
		$data['tambahan_penilaian'] = $this->Pemantauan_model->getTambahanPenilaian($data['user']['area']);		
		if ($this->session->userdata('user')['role'] <= 3) {
			$data['area'] = $this->Pegawai_model->getAllArea();
		}else{
			$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		}
		$data['myArea'] = $data['area']['nama'];		
		$bulan = $this->session->flashdata('bulan');
		$tahun = $this->session->flashdata('tahun');
		if ($_POST != null) {
			if ($_POST['bulan'] == "") {
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Mohon pilih data bulan !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('pemantauan');
			}
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($_POST['bulan'], $_POST['tahun']);
			$monthNum  = $_POST['bulan'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $_POST['tahun'];
		}else if($bulan != null && $tahun != null){
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($bulan, $tahun);
			$monthNum  = $bulan;
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $tahun;
		}else{
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth(date('m'), date('Y'));
			$data['bulan'] = '';			
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('pemantauan/index');
		$this->load->view('templates/sys/footer');
		// die;
		// echo "Dashboard untuk Admin";
	}

	public function control($area)
	{
		$data['title'] = 'Pemantauan';
		$data['navtitle'] = '6. Pemantauan';
		if ($this->session->userdata('user')['role'] == 5) {
			$data['navtitle'] = '3. Pemantauan';			
		}
		$data['user'] = $this->session->userdata('user');
		$data['evidence'] = $this->Pemantauan_model->getAllEvidence();
		$data['penilaian'] = $this->Pemantauan_model->getPenilaianByArea($area);
		$data['tambahan_penilaian'] = $this->Pemantauan_model->getTambahanPenilaian($area);
		$data['area'] = $this->Pegawai_model->getAllArea();
		$data['myArea'] = $this->Pegawai_model->getAreaById($area)['nama'];		
		$data['thisArea'] = $this->Pegawai_model->getAreaById($area);		
		$data['iscontrol'] = "true";
		$bulan = $this->session->flashdata('bulan');
		$tahun = $this->session->flashdata('tahun');
		// die;
		if ($_POST != null) {
			if ($_POST['bulan'] == "") {
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Mohon pilih data bulan !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('pemantauan');
			}
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($_POST['bulan'], $_POST['tahun']);
			$monthNum  = $_POST['bulan'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $_POST['tahun'];
		}else if($bulan != null && $tahun != null){
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($bulan, $tahun);
			$monthNum  = $bulan;
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $tahun;
		}else{
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth(date('m'), date('Y'));
			$data['bulan'] = '';			
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('pemantauan/index');
		$this->load->view('templates/sys/footer');
		// echo "Dashboard untuk Admin";
	}

	// SELECTOR APAKAH $_POST DARI MODAL TAMBAH EVIDENCE BENAR
	public function tambahEvidence()
	{
		$this->form_validation->set_rules('penilaian', 'Penilaian', 'required');
		$this->form_validation->set_rules('tanggal_upload', 'Tanggal Upload', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Evidence gagal diupload! Mohon lengkapi formulir dan pastikan hanya file pdf yang anda upload !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			redirect('pemantauan');			
		}else{
			$this->upload_evidence();					
		}
	}

	// FUNGSI UNTUK UPLOAD EVIDENCE DAN MENAMBAHKAN DATANYA KE DB
	public function upload_evidence()
	{
		$config['upload_path'] = './assets/docs/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']     = '0';
		// load library upload
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('evidence')) {
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Berkas gagal diupload ! ' . $error . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			if ($this->input->post('control') == 'true') {
				redirect('pemantauan/control/'.$this->input->post('area'));				
			}else{
				redirect('pemantauan');
			}
		} else {
			$result = $this->upload->data();
			$data_evidence = array(
				'file' 			=> $result['file_name'],
				'penilaian'		=> $this->input->post('penilaian', true),
				'upload_by'		=> $this->session->userdata('user')['id'],
				'date_uploaded'	=> $this->input->post('tanggal_upload', true),
				'keterangan'	=> $this->input->post('keterangan', true),
				'status'		=> 'Pending'
			);
			$this->Pemantauan_model->tambahDataEvidence($data_evidence);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">Berkas berhasil diupload !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			$bulan = date("m",strtotime($this->input->post('tanggal_upload', true)));
			$tahun = date("Y",strtotime($this->input->post('tanggal_upload', true)));
			$this->session->set_flashdata('bulan', $bulan);
			$this->session->set_flashdata('tahun', $tahun);
			if ($this->input->post('control') == 'true') {
				redirect('pemantauan/control/'.$this->input->post('area'));				
			}else{
				redirect('pemantauan');
			}
		}
	}
	
	// MENAMPILKAN HALAMAN VALIDASI DENGAN SEMUA AREA
	public function validasi_all()
	{
		$data['title'] = 'Validasi Evidence';		
		$data['navtitle'] = '5. Validasi Evidence';
		if ($this->session->userdata('user')['role'] == 5) {
			$data['navtitle'] = '2. Validasi Evidence';			
		}
		$data['user'] = $this->session->userdata('user');
		$data['area'] = $this->Pegawai_model->getAllArea();
		$data['penilaian'] = $this->Pemantauan_model->getAllPenilaian();
		if ($_POST != null) {
			if ($_POST['bulan'] == "") {
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Mohon pilih data bulan !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('pemantauan');
			}
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($_POST['bulan'], $_POST['tahun']);
			$monthNum  = $_POST['bulan'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $_POST['tahun'];
		}else{
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth(date('m'), date('Y'));
			$data['bulan'] = '';			
			$data['tahun'] = date('Y');			
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('pemantauan/validasi_all', $data);
		$this->load->view('templates/sys/footer');
	}

	// MENAMPILKAN HALAMAN VALIDASI BERDASARKAN AREA YANG DIPILIH
	public function validasi($area)
	{
		$thisArea = $this->Pegawai_model->getAreaById($area);
		$data['title'] = 'Validasi Evidence';
		$data['navtitle'] = '5. Validasi Evidence';
		if ($this->session->userdata('user')['role'] == 5) {
			$data['navtitle'] = '2. Validasi Evidence';			
		}
		$data['user'] = $this->session->userdata('user');
		$data['area'] = $this->Pegawai_model->getAllArea();
		$data['thisArea'] = $thisArea;		
		$data['penilaian'] = $this->Pemantauan_model->getPenilaianByArea($area);
		if ($_POST != null) {
			if ($_POST['bulan'] == "") {
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Mohon pilih data bulan !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('pemantauan');
			}
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth($_POST['bulan'], $_POST['tahun']);
			$monthNum  = $_POST['bulan'];
			$dateObj   = DateTime::createFromFormat('!m', $monthNum);
			$monthName = $dateObj->format('F'); // March
			$data['bulan'] = $monthName;
			$data['tahun'] = $_POST['tahun'];
		}else{
			$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonth(date('m'), date('Y'));
			$data['bulan'] = '';			
		}
		// $data['evidence'] = $this->Pemantauan_model->getEvidenceByArea($area);
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('pemantauan/validasi', $data);
		$this->load->view('templates/sys/footer');
	}

	// FUNGSI VALIDASI EVIDENCE UNTUK VALIDATOR
	public function validasiEvidence(){
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Evidence gagal divalidasi! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
		}else{
			$result = $this->Pemantauan_model->validasi_evidence();
			if ($result > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Evidence berhasil divalidasi!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Evidence gagal divalidasi! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}
		}
		if ($this->input->post('validasi_all') == 'true') {
			redirect('pemantauan/validasi_all');			
		}else{
			redirect('pemantauan/validasi/'.$this->input->post('area', true));			
		}
	}

	// FUNGSI UNTUK REVISI EVIDENCE
	public function revisiEvidence()
	{
		// var_dump($_POST);
		// var_dump($_FILES);
		$config['upload_path'] = './assets/docs/';
		$config['allowed_types'] = 'pdf';
		$config['max_size']     = '0';
		$old_evidence = $this->input->post('old_evidence');		
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('evidence')) {
			$error = $this->upload->display_errors();
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Berkas revisi gagal diupload ! ' . $error . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('pemantauan');
		} else {
			$result = $this->upload->data();
			unlink(FCPATH . 'assets/docs/' . $old_evidence);			
			$this->Pemantauan_model->revisi_evidence();
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">Berkas revisi berhasil diupload !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			if ($this->input->post('control') == 'true') {
				redirect('pemantauan/control/'.$this->input->post('area'));				
			}else{
				redirect('pemantauan');
			}
		}
	}

	public function hapusEvidence($area ,$id)
	{
		$fileinfo 	= $this->Pemantauan_model->getEvidencebyId($id);
		$result 	= $this->Pemantauan_model->hapus_evidence($id);
		if ($result > 0) {
			unlink(FCPATH . 'assets/docs/' . $fileinfo['file']);
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show mt-3" role="alert">Berkas berhasil dihapus !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Berkas gagal dihapus !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
		}
		redirect('pemantauan/control/'.$area);				
	}
	// public function getPemantauanByMY()
	// {
	// 	$data['user'] = $this->session->userdata('user');
	// 	$data['evidence'] = $this->Pemantauan_model->getEvidenceByMonthYear();
	// 	$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
	// 	if ($data['user']['role'] < 5) {
	// 		$data['penilaian'] = $this->Pemantauan_model->getAllPenilaian();
	// 		$data['myArea'] = '';
	// 	}else{
	// 		$data['penilaian'] = $this->Pemantauan_model->getPenilaianByArea($data['user']['area']);
	// 	}
	// 	echo json_encode($data);
	// }
}