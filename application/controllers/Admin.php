<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');			
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');
		$this->load->model('Pemantauan_model');
		is_logged_in();
	}
		
	public function index()
	{
		$data['title'] = 'Simantap';
		$data['navtitle'] = '1. Dashboard';
		$data['pengguna'] = $this->Pengguna_model->getAllPengguna();
		$data['pegawai'] = $this->Pegawai_model->getAllPegawai();	
		$data['namearea'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		$data['area'] = $this->Pegawai_model->getAllArea();
		$data['user'] = $this->session->userdata('user');
		$data['prosentase'] = $this->Pemantauan_model->getDashboardProsentase();
		if ($_POST != null) {
			$data['tahun'] = $this->input->post('tahun', true);
		}else{
			$data['tahun'] = date('Y');
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/sys/footer');
		// echo "Dashboard untuk Admin";
	}
}