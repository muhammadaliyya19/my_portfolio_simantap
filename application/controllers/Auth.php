<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');		
	}
		
	public function index()
	{
		if ($this->session->userdata('user') != NULL) {		
			if ($this->session->userdata('user')['role'] == "6") {
				redirect('user');
			}else{
				redirect('admin');
			}	
		}
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = "SIMANTAP";
			$this->load->view('templates/auth/header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth/footer');
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->Pengguna_model->getPenggunaByUsername($username);		
		if ($user) {
				$pegawai = $this->Pegawai_model->getPegawaiByNIP($user['nip_pegawai']);
				$jabatan = $this->Pegawai_model->getJabatanById($pegawai['jabatan']);				
				$role 	 = $this->Pengguna_model->getRoleById($user['role']);				
				if ($password == $user['password']) {
					$data = [
						'username' 	=> $user['username'],
						'nama' 		=> $pegawai['nama'],
						'nip' 		=> $user['nip_pegawai'],
						'role' 		=> $user['role'],
						'nama_role' => $role['nama'],
						'area' 		=> $pegawai['area'],
						'jabatan' 	=> $jabatan['nama_jabatan'],
						'id' 		=> $user['id']
					];
					$this->session->set_userdata('user', $data);
					if ($data['role'] == "6") {
						redirect('user');
					}else{
						redirect('admin');
					}
				}else{
					//Jika password salah
					$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">Wrong password!</div>');
					redirect('auth');
				}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">USername is not registered!</div>');
			redirect('auth');
		}
	}

	public function logout()
	{
		session_unset(); 
		session_destroy(); 
		$this->session->set_flashdata('message','<div class="alert alert-success" role="alert">You have been logout!</div>');
		redirect('auth');
	}	

	public function blocked()
	{
		$data['title'] = 'Access Blocked ! ';
		$this->load->view('templates/auth/header', $data);
		$this->load->view('auth/blocked');
		$this->load->view('templates/auth/footer');
	}

	public function notfound()
	{
		$data['title'] = 'Page not Found ! ';
		$this->load->view('templates/pos/header', $data);
		$this->load->view('templates/pos/navbar', $data);
		$this->load->view('templates/pos/sidebar', $data);
		$this->load->view('auth/notfound', $data);
		$this->load->view('templates/pos/footer');
	}

	public function forgotpassword($value = '')
	{
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = 'Forgot Password';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/forgotpassword');
			$this->load->view('templates/auth_footer');
		} else {
			$email = $this->input->post('email');
			$user = $this->db->get_where('user', ['email' => $email])->row_array();

			if ($user) {
				if ($user['is_active'] != 0) {
					# code...
					$token = base64_encode(random_bytes(32));
					$user_token = [
						'email' => $email,
						'token' => $token,
						'date_created' => time()
					];
					$this->db->insert('user_token', $user_token);
					_sendEmail($token, 'forgot');
					$this->session->set_flashdata('message', '<div class="alert alert-success mt-3" role="alert">Silakan cek email untuk reset password !</div>');
					redirect('auth/forgotpassword');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email is not activated!</div>');
					redirect('auth/forgotpassword');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email tidak terdaftar!</div>');
				redirect('auth/forgotpassword');
			}
		}
	}

	public function resetpassword()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();

		if ($user) {
			$user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
			if ($user_token) {
				// session_start();
				$_SESSION['user']['reset_email'] = $email;
				$this->resetPwd();
				// $this->db->set('is_active', 1);
				// $this->db->where('email', $email);
				// $this->db->update('user');

				// $this->db->delete('user_token',['email',$email]);
				// $this->session->set_flashdata('message','<div class="alert alert-success" role="alert">'.$email.' Telah aktif. Silakan login!</div>');	
				//redirect('auth');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Reset password gagal !  Token tidak valid !</div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Reset password gagal !  Email tidak terdaftar !</div>');
			redirect('auth');
		}
	}

	public function resetPwd()
	{
		if (!isset($_SESSION['user']['reset_email'])) {
			redirect('auth');
		}
		$this->form_validation->set_rules('password1', 'New Password', 'required|trim|min_length[8]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Repeat Password', 'required|trim|min_length[8]|matches[password1]');
		if ($this->form_validation->run() == FALSE) {
			$data['title'] = 'Forgot Password';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/resetpwd');
			$this->load->view('templates/auth_footer');
		} else {
			$password = password_hash($this->input->post('password1'), PASSWORD_DEFAULT);
			$email    = $_SESSION['user']['reset_email'];
			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('user');
			$this->db->delete('user_token', ['email' => $_SESSION['user']['reset_email']]);
			session_unset('user');
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Reset password berhasil !  Silakan Login !</div>');
			redirect('auth');
		}
	}
}
