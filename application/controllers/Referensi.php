<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Referensi extends CI_Controller
{
	// SEGALA FUNGSI UNTUK MENU REFERENSI
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('form_validation');			
		$this->load->model('Pegawai_model');
		$this->load->model('Pengguna_model');
		is_logged_in();
	}
		
	// SEMUA FUNGSI UNTUK ENTITAS PEGAWAI

	public function pegawai()
	{
		$data['title'] = 'Pegawai';
		$data['navtitle'] = '2. Pegawai';
		$data['user'] = $this->session->userdata('user');
		$data['jabatan'] = $this->Pegawai_model->getAllJabatan();
		$data['pegawai'] = $this->Pegawai_model->getAllPegawai();
		if ($data['user']['role'] != 5) {
			$data['area'] = $this->Pegawai_model->getAllArea();
		}else{
			$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar', $data);
		$this->load->view('pegawai/index', $data);
		$this->load->view('templates/sys/footer');
		// echo "Dashboard untuk Admin";
	}	

	public function tambahPegawai()
	{
		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('golongan', 'Golongan', 'required');
		$this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('area', 'Area', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('keaktifan', 'Keaktifan', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pegawai gagal ditambahkan! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			redirect('referensi/pegawai');			
		}else{
			if ($this->Pegawai_model->tambahDataPegawai() > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Data pegawai telah ditambahkan! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pegawai gagal ditambahkan! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}
			redirect('referensi/pegawai');			
		}
	}

	public function editPegawai()
	{
		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('golongan', 'Golongan', 'required');
		$this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('keaktifan', 'Keaktifan', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pegawai gagal diedit! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			redirect('referensi/pegawai');			
		}else{
			if ($this->Pegawai_model->editDataPegawai() > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Data pegawai telah diedit! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pegawai gagal diedit! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}
			redirect('referensi/pegawai');			
		}
	}

	public function hapusPegawai($id)
	{
		if ($this->Pegawai_model->hapusDataPegawai($id) > 0) {
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Data pegawai telah dihapus ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			redirect('referensi/pegawai');
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-warning alert-dismissible fade show mt-3" role="alert">Data pegawai gagal dihapus ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			redirect('referensi/pegawai');
		}
	}

	public function getPegawai()
	{
		echo json_encode($this->Pegawai_model->getPegawaiById($_POST['id']));
	}

	// SEMUA FUNGSI UNTUK ENTITAS PENGGUNA

	public function pengguna()
	{
		$data['title'] = 'Pengguna';
		$data['navtitle'] = '3. Pengguna';
		$data['user'] = $this->session->userdata('user');
		$data['pengguna'] = $this->Pengguna_model->getAllPengguna();
		$data['role'] = $this->Pengguna_model->getAllRole();
		$data['pegawai'] = $this->Pegawai_model->getAllPegawai();		
		if ($data['user']['role'] != 5) {
			$data['area'] = $this->Pegawai_model->getAllArea();
		}else{
			$data['area'] = $this->Pegawai_model->getAreaById($this->session->userdata('user')['area']);
		}
		$this->load->view('templates/sys/header', $data);
		$this->load->view('templates/sys/navbar');
		$this->load->view('templates/sys/sidebar');
		$this->load->view('pengguna/index');
		$this->load->view('templates/sys/footer');
		// echo "Dashboard untuk Admin";
	}

	public function tambahPengguna()
	{
		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('blokir', 'Status Blokir', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pengguna gagal ditambahkan! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			redirect('referensi/pengguna');			
		}else{
			if ($this->Pengguna_model->tambahDataPengguna() > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Data pengguna telah ditambahkan! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pegawai gagal ditambahkan! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}
			redirect('referensi/pengguna');			
		}
	}

	public function editPengguna()
	{
		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('blokir', 'Status Blokir', 'required');
		if ($this->form_validation->run() == FALSE) {			
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pengguna gagal diedit! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');						
			redirect('referensi/pengguna');			
		}else{
			if ($this->Pengguna_model->editDataPengguna() > 0) {
				$this->session->set_flashdata('message','<div class="alert alert-success alert-dismissible fade show" role="alert">Data pengguna telah diedit! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show" role="alert">Data pengguna gagal diedit! Mohon lengkapi formulir !<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');										
			}
			redirect('referensi/pengguna');			
		}
	}

	public function hapusPengguna($id)
	{
		if ($this->Pengguna_model->hapusDataPengguna($id) > 0) {
			$this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">Data pengguna telah dihapus ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			redirect('referensi/pengguna');
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-warning alert-dismissible fade show mt-3" role="alert">Data pengguna gagal dihapus ! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');	
			redirect('referensi/pengguna');
		}
	}

	public function getPengguna()
	{
		echo json_encode($this->Pengguna_model->getPenggunaById($_POST['id']));
	}
}