$(function() {	
	// FOR NEW PEGAWAI MODAL
	$('.add_pegawai').on('click', function () {
		$('.modal-title').html('Tambah Pegawai');		
		$('.modal-pegawai form').attr('action', 'http://localhost/si-mantap/referensi/tambahPegawai');
		$('.modal-body button[type=submit]').html('Simpan');				
		$('#nip').val('');
		$('#namapegawai').val('');		
		$('#golongan').val('');
		$('#pangkat').val('');
		$('#ruang').val('');
		$('#area').val('');
		$('#jabatan').val('');
		$('#alamat').val('');
		$('#keaktifan').val('');
	});

	// UPDATE PEGAWAI FUNCTION
	$('body').on('click','.edit_pegawai',function(){	
		$('.modal-title').html('Edit Pegawai');		
		const id = $(this).data('id');
		$('.modal-body button[type=submit]').html('Edit');		
		$('.modal-pegawai form').attr('action', 'http://localhost/si-mantap/referensi/editPegawai');
				console.log(id);
		$.ajax({
			url: 'http://localhost/si-mantap/referensi/getPegawai',
			data: { id: id },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				// $('#id').val(data.id);
				console.log(data);
				$('#id').val(data.id);
				$('#nip').val(data.NIP);
				$('#namapegawai').val(data.nama);
				$('#golongan').val(data.golongan);
				$('#pangkat').val(data.pangkat);
				$('#ruang').val(data.ruang);
				$('#area').val(data.area);
				$('#jabatan').val(data.jabatan);
				$('#alamat').val(data.alamat);
				$('#keaktifan').val(data.aktif);				
			}
		});
	});

	// FOR USER MODAL
	$('.add_pengguna').on('click', function () {
		$('.modal-title').html('Tambah Pengguna');		
		$('.modal-pengguna form').attr('action', 'http://localhost/si-mantap/referensi/tambahPengguna');
		$('.modal-body button[type=submit]').html('Simpan');				
		$("#is_validator").prop("checked", false);
		$('#nip').val('');
		$('#username').val('');		
		$('#password').val('');
		$('#retypepwd').val('');
		$('#email').val('');
		$('#blokir').val('');
		$('#role').val('');
	});

	// UPDATE USER FUNCTION
	$('body').on('click','.edit_pengguna',function(){	
		$('.modal-title').html('Edit Pengguna');		
		$('.modal-pengguna form').attr('action', 'http://localhost/si-mantap/referensi/editPengguna');
		$('.modal-body button[type=submit]').html('Edit');		
		const id = $(this).data('id');
		$.ajax({
			url: 'http://localhost/si-mantap/referensi/getPengguna',
			data: { id: id },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				console.log(data);
				if (data.is_validator == '1') {
					$("#is_validator").prop("checked", true);
				}else{
					$("#is_validator").prop("checked", false);
				}
				$('#id').val(data.id);
				$('#nip').val(data.nip_pegawai);
				$('#username').val(data.username);		
				$('#role').val(data.role);		
				$('#password').val(data.password);
				$('#retypepwd').val(data.password);
				$('#email').val(data.email);
				$('#blokir').val(data.blokir);				
			}
		});
	});

	// ADD MEMBER FUNCTION
	$('body').on('click','.add_evidence', function () {
		const idpenilaian = $(this).data('id');
		const idevidence = $(this).data('evidence');
		const status_evidence = $(this).data('status');		
		if (status_evidence == "Revisi") {
			$('.modal-title').html('Upload Revisi Evidence');
			$('.modal-evidence form').attr('action', 'http://localhost/si-mantap/pemantauan/revisiEvidence');
			$('.custom-file-label').html($(this).data('evname'));			
			$('#old_evidence').val($(this).data('evname'));
			$('.tanggal_label').html('Tanggal Upload Revisi');
			$('#currentevidence').val(idevidence);
		}else if(status_evidence == "Pending"){
			alert("Berkas masih menunggu validasi, belum diperlukan update!");
			$('.modal-title').html('Evidence Saat Ini');
			$('.modal-evidence form').attr('action', 'http://localhost/si-mantap/pemantauan/tambahEvidence');			
			$('.tanggal_label').html('Tanggal');			
			$('.custom-file-label').html($(this).data('evname'));			
			$('#old_evidence').val($(this).data('evname'));
			$('.tanggal_label').html('Tanggal');
			$('#currentevidence').val(idevidence);
		}else if(status_evidence == "Diterima"){
			alert("Berkas sudah sesuai, tidak perlu update lagi!");
			$('.modal-evidence form').attr('action', 'http://localhost/si-mantap/pemantauan/tambahEvidence');			
			$('.tanggal_label').html('Tanggal');			
			$('.modal-title').html('Upload Evidence');
			$('#file_evidence').val('');
			$('#keterangan').val('');
			$('.custom-file-label').html('Choose file . . .');
		}else{
			$('.modal-evidence form').attr('action', 'http://localhost/si-mantap/pemantauan/tambahEvidence');			
			$('.tanggal_label').html('Tanggal');			
			$('.modal-title').html('Upload Evidence');
			$('#penilaian_doc').val(idpenilaian);
			$('#file_evidence').val('');
			$('#keterangan').val('');
			$('.custom-file-label').html('Choose file . . .');
		}
	});

	$('.custom-file-input').on('change', function () {
		let fileName = $(this).val().split('\\').pop();
		$('.custom-file-label').html(fileName);
	});

	$('#golongan').on('change', function () {
		var gol = $(this).val();
		if (gol == "IV") {
			$('#ruang').append('<option value="e">e</option>');
		}else{
			$('#ruang').empty();
			$('#ruang').append('<option value="">Pilih Ruang</option>');
			$('#ruang').append('<option value="a">a</option>');
			$('#ruang').append('<option value="b">b</option>');
			$('#ruang').append('<option value="c">c</option>');
			$('#ruang').append('<option value="d">d</option>');
		}		
	});

	$('body').on('click','.showdoc',function () {
		const filename = $(this).data('docname');
		var object = "http://localhost/si-mantap/assets/docs/" + filename;
		$(".modal-content iframe").attr('src', 'http://localhost/si-mantap/assets/docs/' + filename);
	});

	$('body').on('click','.validasi_evidence',function () {
		const id 	= $(this).data('id');
		const area  = $(this).data('area');
		$('#evidence_id').val(id);
		$('#evi_area').val(area);
	});

	$('#bulan').on('change', function () {
		var bulan = $(this).val();
		var tahun = $('#tahun').val();
		var my = tahun + '-' + bulan;
		$.ajax({
			url: 'http://localhost/si-mantap/pemantauan/getPemantauanByMY',
			data: { my: my },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				console.log(data);				
			}
		});
	});

	$('#tahun').on('change', function () {
		var tahun = $(this).val();
		var bulan = $('#bulan').val();
		var my = tahun + '-' + bulan;
		$.ajax({
			url: 'http://localhost/si-mantap/pemantauan/getPemantauanByMY',
			data: { my: my },
			method: 'post',
			dataType: 'json',
			success: function (data) {
				console.log(data);				
			}
		});
	});
});	