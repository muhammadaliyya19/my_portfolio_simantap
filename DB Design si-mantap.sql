-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2020 at 04:45 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `si-mantap`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `nama`, `date_created`) VALUES
(1, 'Top Management / Pimpinan', '2020-10-22 15:57:18'),
(2, 'Hakim', '2020-10-22 15:57:18'),
(3, 'Panmud Hukum', '2020-10-22 15:57:18'),
(4, 'Panmud Perdata', '2020-10-22 15:57:18'),
(5, 'Panmud Pidana', '2020-10-22 15:57:18'),
(6, 'Panmud Tipikor', '2020-10-22 15:57:18'),
(7, 'PanItera Pengganti', '2020-11-11 07:12:45'),
(8, 'Rencana Program Anggaran', '2020-11-11 07:01:40'),
(9, 'Kepegawaian dan TI', '2020-11-11 07:01:56'),
(10, 'Tata Usaha dan Rumah Tangga', '2020-11-12 14:58:58'),
(11, 'Keuangan dan Pelaporan', '2020-11-13 10:32:06');

-- --------------------------------------------------------

--
-- Table structure for table `evidence`
--

CREATE TABLE `evidence` (
  `id` int(11) NOT NULL,
  `file` text NOT NULL,
  `penilaian` int(11) NOT NULL,
  `upload_by` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `date_uploaded` date NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `evidence`
--

INSERT INTO `evidence` (`id`, `file`, `penilaian`, `upload_by`, `keterangan`, `status`, `date_uploaded`, `date_updated`) VALUES
(11, 'panduan-skm-v2.pdf', 42, 16, 'Laporan berkas.', 'Pending', '2020-11-11', '2020-11-11 09:17:37'),
(12, '2018_juklak_lkti.pdf', 1, 1, 'Test refresh.', 'Pending', '2020-10-13', '2020-11-13 13:24:33'),
(13, 'Artikel.pdf', 2, 1, 'Test', 'Pending', '2020-11-13', '2020-11-13 13:30:11'),
(14, '3478_latihan_soal.pdf', 1, 1, 'Refresh 2', 'Pending', '2020-10-13', '2020-11-13 13:31:51'),
(15, '670.pdf', 2, 1, 'Test oktober refresh', 'Pending', '2020-10-13', '2020-11-13 14:13:29'),
(16, '14320-LoA-13945-1-18-20200722.pdf', 3, 1, 'November dari oktober', 'Pending', '2020-11-13', '2020-11-13 14:14:46'),
(17, '103110080_-_Bab_II.pdf', 1, 1, 'September', 'Pending', '2020-09-13', '2020-11-13 14:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `nama_jabatan` varchar(64) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`id`, `nama_jabatan`, `date_created`) VALUES
(1, 'Ketua', '2020-10-26 11:44:51'),
(2, 'Wakil Ketua', '2020-10-26 11:44:56'),
(3, 'Sekretaris', '2020-10-26 14:59:00'),
(4, 'Panitera', '2020-10-26 14:59:11'),
(5, 'Panitera Muda Perdata', '2020-10-26 14:59:23'),
(6, 'Panitera Muda Pidana', '2020-10-26 15:00:04'),
(7, 'Panitera Muda Hukum', '2020-10-26 15:12:39'),
(8, 'Panitera Muda Khusus Tipikor', '2020-10-26 15:00:33'),
(9, 'Kepala Bagian Umum dan Keuangan', '2020-10-26 15:02:07'),
(10, 'Kepala Bagian Perencanaan dan Kepegawaian', '2020-10-26 15:02:37'),
(11, 'Kepala Sub Bagian Rencana Program dan Anggaran', '2020-10-26 15:02:20'),
(12, 'Kepala Sub Bagian Keuangan dan Pelaporan', '2020-10-26 12:36:54'),
(13, 'Kepala Sub Bagian Umum dan Rumah Tangga', '2020-10-26 15:03:19'),
(14, 'Kepala Sub Bagian Kepegawaian dan Teknologi Informasi', '2020-10-26 15:04:10'),
(15, 'Pantera Pengganti', '2020-10-26 15:06:18'),
(16, 'Pranata Peradilan', '2020-10-26 15:06:18'),
(17, 'Fungsional Arsiparis', '2020-10-26 15:06:18'),
(18, 'Fungsional Pustakawan', '2020-10-26 15:06:18'),
(19, 'Fungsional Pranata Komputer', '2020-10-26 15:06:18'),
(20, 'Fungsional Bendahara', '2020-10-26 15:06:18'),
(21, 'Hakim Tinggi', '2020-10-26 11:44:51'),
(22, 'Staf Panmud Pidana', '2020-11-03 22:39:27'),
(23, 'Staf Panmud Perdata', '2020-11-03 22:39:27'),
(24, 'Staf Panmud Hukum', '2020-11-03 22:39:27'),
(25, 'Staf Panmud Tipikor', '2020-11-03 22:39:27'),
(26, 'Staf Sub Bagian Kepegawaian dan TI', '2020-11-03 22:39:27'),
(27, 'Staf Sub Bagian Rencana Program Anggaran', '2020-11-03 22:39:27'),
(28, 'Staf Sub Bagian Tata Usaha dan Rumah Tangga', '2020-11-03 22:39:27'),
(29, 'Staf Sub Bagian Keuangan dan Pelaporan', '2020-11-03 22:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `NIP` varchar(18) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `golongan` varchar(10) NOT NULL,
  `pangkat` text NOT NULL,
  `ruang` varchar(1) NOT NULL,
  `jabatan` varchar(10) NOT NULL,
  `area` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `aktif` varchar(10) NOT NULL,
  `date_updated` date NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `NIP`, `nama`, `golongan`, `pangkat`, `ruang`, `jabatan`, `area`, `alamat`, `aktif`, `date_updated`, `date_created`) VALUES
(1, '199807202022061025', 'Administrator', 'III', 'Pembina', 'a', '10', 1, 'Kandangan', 'Ya', '2020-10-26', '2020-10-26'),
(2, '123456789987654321', 'RESPATUN WISNU WARDOYO, S.H.', 'III', 'Pembina', 'd', '1', 1, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(3, '987456321654789123', 'SUDIYANTO, S.H., M.H.', 'IV', 'Penata Tingkat I', 'c', '2', 1, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(4, '321478965123456789', 'YUNDA HASBI, SH., M.H.', 'III', 'Pengatur Muda Tingkat I', 'c', '4', 2, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(5, '658974123123456789', 'Drs. SUTIKNO, M.H.', 'III', 'Penata Muda Tingkat I', 'c', '3', 1, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(6, '123654879458796321', 'SUNIYANTA, S.H., M.H.', 'III', 'Pengatur', 'c', '6', 5, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(7, '153624789789456123', 'POEDJI RAHARDJO, S.H.', 'III', 'Pembina', 'c', '5', 4, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(8, '751953123456789123', 'CHRISTANTO PIDJIONO, S.H., M.H.', 'III', 'Pembina', 'a', '7', 3, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(9, '953153759789456123', 'AIF SAIFUDAULLAH, S.H., M.H.', 'III', 'Pembina', 'c', '8', 6, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(10, '456957896321456789', 'NURHAYANI, S.H., M.H.', 'III', 'Pengatur Muda Tingkat I', 'c', '9', 9, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(11, '452178963369852147', 'NEVI MUGIA SANTOSA, S.E., M.Si.', 'II', 'Pengatur Tingkat I', 'd', '10', 8, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(12, '458796369456789123', 'Drs. IDRIS PASLUNI', 'III', 'Pengatur Tingkat I', 'b', '13', 9, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(13, '456965874123456789', 'NURFITRI, S. Kom.', 'III', 'Pengatur Tingkat I', 'c', '12', 9, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(14, '582936714456789123', 'SRI DIAN GUNARSIH, S.E., Ak.', 'III', 'Penata', 'a', '11', 11, 'Banten', 'Ya', '2020-11-11', '2020-10-26'),
(15, '486957123963258741', 'HERRY SETIYAWAN, S.T.', 'II', 'Penata Muda', 'c', '14', 8, 'Banten', 'Ya', '2020-10-26', '2020-10-26'),
(16, '123456789654321978', 'Dr. H. Muhammad Syarifuddin, S.H, M.H.', 'IV', 'Pembina Utama Madya', 'b', '21', 2, 'Jakarta', 'Ya', '2020-10-26', '2020-10-26'),
(17, '18237130', 'Muhammad A Ilmi', 'III', 'Penata Tingkat I', 'd', '3', 1, 'Kandangan - Kediri', 'Ya', '2020-11-06', '2020-11-05');

-- --------------------------------------------------------

--
-- Table structure for table `penilaian`
--

CREATE TABLE `penilaian` (
  `id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `nama` text NOT NULL,
  `tugas` int(11) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `penilaian`
--

INSERT INTO `penilaian` (`id`, `area`, `nama`, `tugas`, `date_created`) VALUES
(1, 1, 'Struktur TIM PMPT dan TUSI ( Update SK disosialisasikan rapat dan monev)', 99, '2020-11-11 08:07:10'),
(2, 1, 'Pelaksanaan Pengawasan dan Pembinaan sesuai dengan PERMA No. 7 Tahun 2016 (disosialisasikan Syrat Izin Cuti dll Monev)', 99, '2020-11-11 08:07:11'),
(3, 1, 'Pelaksanaan Pengawasan Melekat dan Pembinaan sesuai dengan PERMA No. 8 Tahun 2016 (disosialisasikan pembinaan dan pengawasan melekat monev pemberian sanksi atau penghargaan', 99, '2020-11-11 08:07:11'),
(4, 1, 'Pelaksanaan Pengawasan dan Pembinaan sesuai dengan PERMA No. 9 Tahun 2016 ( Sosialisasi sarana pengaduan monev dan tindaklanjut)', 99, '2020-11-11 08:07:11'),
(5, 1, 'Telah dilaksanakan pembagian tugas antara KPT dengan WKPT serta telah bekerja sama dengan baik ( SK Pembagian Tugas)', 9, '2020-11-11 08:07:11'),
(6, 1, 'WKPT sebagai Koordinator Pengawasan ( SK WKPT Sebagai Korwasbid Korwasda Rencana dan jadwal Pengawasan Monev Tindak lanjut pengawasan)', 99, '2020-11-11 08:07:11'),
(7, 1, 'Laporan Tindak Lanjut Pelaksanaan pengawasan Hakim Tinggi Pengawas Daerah', 3, '2020-11-11 08:07:11'),
(8, 1, 'Hakim Pengawas Bidang yang ditunjuk telah melaksanakan tugas pengawasan dan telah memberi petunjuk serta bimbingan yang diperlukan bagi para pejabat struktural maupun pejabat fungsional dan petugas yang terkait. (SK Jadwal Laporan Monev dan tindaklanjut)', 99, '2020-11-11 08:07:11'),
(9, 1, '\"Penetapan Agen Perubahan (Permenpan Nomor: 52 Tahun 2014 dan Permenpan Nomor 27 Tahun 2014) ( SK Tim Penilai dan kriteria penilaian Daftar Riwayat Hidup dan Rekam Jejak Kandidat Agen Perubahan SK Penetapan Agen Perubahan   berita acara penilaian Diperbaharui minimal setiap 6 bulan\"', 9, '2020-11-11 08:07:11'),
(10, 1, 'Penetapan Role Model (Permenpan Nomor: 52 Tahun 2014) Pimpinan berperan sebagai role model dalam pembangunan Zona Integritas  ( SK Tim Penilai dan kriteria penilaian SK Penetapan Role Model berita acara penilaian Ditetapkan minimal 6 bulan', 9, '2020-11-11 08:07:11'),
(11, 1, '\"Pelaksanaan Rapat Manajemen ( Rapat berjenjang tiap bagian sampai rapat dengan pimpinan SK Rapat Berjenjang)\"', 99, '2020-11-11 08:07:11'),
(12, 1, '\"Penandatanganan Pakta Integritas (sesuai PERMENPAN No. 52 Tahun 2014 dan Surat Sekma No. 131/1/SEK/KU.01/6/2016 diperbaharui setiap tahun) \"', 9, '2020-11-11 08:07:11'),
(13, 1, '\"Pedoman Kerja / Manual Mutu: a. Visi dan Misi b. Budaya Kerja c. Motto Pengadilan d. Profil Pengadilan e. Struktur Penjamin Mutu f. Komitmen Bersama g. Rincian Tugas Struktur PMPT\"', 99, '2020-11-11 08:07:11'),
(14, 1, '\"Pelatihan Assesor PT  ( terjadwal minimal dua kali dalam satu tahun diskusi mengenai teknik pelaksanaan Asesmen secara berkala (minimal satu bulan sekali) Evaluasi Pimpinan terhadap pelaksanaan Asesmen oleh Assesor PT\"', 99, '2020-11-11 08:07:11'),
(15, 1, '\"Pelaksanaan tugas asesmen  Asessor Oleh PMPT (jadwal dan rencana assesmen untuk satu tahun dilakukan Penilaian terhadap seluruh kegiatan pada pengadilan negeri melaporkan hasil asesmen kepada koordinator teknikal dan MR Menilai pelaksanaan tindaklanjut PN terhadap hasil temuan\"', 99, '2020-11-11 08:07:11'),
(16, 1, '\"1. Dokumen SAKIP  a.  Indikator Kinerja Utama (IKU)  b.  Rencana Strategis (RENSTRA)  c.  Recana Kinerja Tahunan (RKT)  d.  RENJA - RKAK/L  e.  Perjanjian Kinerja Tahunan (PKT)  f.  Laporan Kinerja Instansi Pemerintah   (LKjIP) (Pengukuran Kinerja = Realisasi :   Target X 100%)  2. Laporan Tahunan\"', 8, '2020-11-11 08:07:11'),
(17, 1, '\"Pembentukan Tim ZI undangan pembentukan tim  kerja (rapat pembentukan tim kerja Berita Acara   SK tim kerja WBK/WBBM Daftar Riwayat Hidup Anggota Tim Kerja Notulen Rapat\"', 99, '2020-11-11 08:07:11'),
(18, 1, '\"Pembangunan Zona Integritas Menuju Wilayah Bebas dari Korupsi dan Wilayah Birokrasi Bersih dan Melayani ( Ada Struktur ZI  Masing masing area sudah membuat rencana kegiatan dan jadwal  melaksanakan penilaian mandiri LKE ZI lengkap dengan data dukung \"', 99, '2020-11-11 08:07:11'),
(19, 1, '\"Monitoring dan evaluasi pembangunan Zona Integritas (Monev dilakukan  setiap bulan sekali Laporan Hasil Pelaksanaan masing- masing Renaksi yang telah dilaksanakan Tindak Lanjut \"', 99, '2020-11-11 08:07:11'),
(20, 1, 'Pelaksanaan Monitoring Pembangunan ZI oleh PT terhadap Pengadilan Negeri - Pengadilan Negeri di Wilayah Hukumnya (monitoring dan pembinaan terhadap pembangunan ZI pada seluruh Pengadilan Negeri )', 99, '2020-11-11 08:07:11'),
(21, 1, 'AREA 1 ZI Manajemen Perubahan (Tim kerja Dokumen rencana pembangunan ZI monev pembangunan WBK/WBBM Program kerja untuk Perubahan pola pikir dan budaya kerja SK Penetapan Role Model ', 99, '2020-11-11 08:07:11'),
(22, 1, 'AREA 2 ZI Penataan Tatalaksana (SOP lengkap Monev setiap tahun e-office dan inovasi Keterbukaan informasi publik', 99, '2020-11-11 08:07:11'),
(23, 1, '\"AREA 3 ZI Penataan SDM (Perencanaan kebutuhan pegawai Pola mutasi internal Data Diklat Penetapan kinerja individu Monev SIKEP  Aturan disiplin\"', 99, '2020-11-11 08:07:11'),
(24, 1, 'AREA 4 ZI Penguatan Akuntabilitas (Keterlibatan pimpinan ( pimpinan terlibat dalam penyusunan Pengelolaan Akuntabilitas)', 99, '2020-11-11 08:07:11'),
(25, 1, 'AREA 5 Penguatan Pengawasan (SK Pengendalian gratifikasi monev Penerapan SPIP (MR) Sarana pengaduan masyarakat. Whistleblowing system/ sudah dibentuk Tim penanganan pengaduan dan sudah bekerja dengan baik SK Pelaksanaan benturan kepentingan Monev', 99, '2020-11-11 08:07:11'),
(26, 1, '\"AREA 6 Penguatan Kualitas Pelayanan (SK Standar pelayanan publik dan buktinya di PTSP Pelatihan Budaya pelayanan prima di PTSP Monev PTSP  inovasi pelayanan Penilaian kepuasan terhadap pelayanan (IKM) dan sudah dimonev Hasil survei harus dapat diakses masyarakat /dipublikasikan\"', 99, '2020-11-11 08:07:11'),
(27, 1, 'Analisa dan pengembangan kompetensi berdasarkan hasil survei kepuasan masyarakat terhadap seluruh Pengadilan Negeri di Wilayah Pengadilan Tinggi .  ( Monev )', 3, '2020-11-11 08:07:11'),
(28, 1, 'Maklumat Pelayanan sesuai Permenpan No. 15 Tahun 2014 Tentang Pedoman Standar Pelayanan. (Maklumat pelayanan sudah dideklarasikan sesuai dengan isi PERMENPAN No.15 Tahun 2014 Tentang Pedoman Standar Pelayanan disosialisasikan monev ditindaklanjuti', 99, '2020-11-11 08:07:11'),
(29, 1, 'Standar Pelayanan Pengadilan sesuai SK. KMA No. 026/KMA/SK/II/2012 dan Permenpan Nomor 15 Tahun 2014 (Sudah ada Standar pelayanan yang memuat unsur- unsur  : dasar hukum persyaratan prosedur waktu layananbiayaproduk dan pengelolaan pengaduan sudah tepat dan sudah ditetapkan disosialisasikan dilaksanakan monev ditindaklanjuti.', 99, '2020-11-11 08:07:11'),
(30, 1, 'Penanganan Benturan Kepentingan (SK Sekma 59A/Sek/SK/11/2014) (ada SK Tim Pengendalian Gratifikasi ada SK tatacara Penanganan Gratifikasi disosialisasikan oleh KPT monev dan ditindaklanjuti', 99, '2020-11-11 08:07:11'),
(31, 1, 'Pengendalian gratifikasi (SK Tim Pengendalian Gratifikasi SK tatacara Penanganan Gratifikasi disosialisasikan oleh KPT Monev dan ditindaklanjuti', 99, '2020-11-11 08:07:12'),
(32, 1, '\"Penerapan Budaya Kerja a. Pelayanan yang optimal (kecepatan dan     ketepatan penangan perkara) b. Kedisiplinan c. Kerjasama d. 5R dan 3S e. Peraturan-peraturan baru di lingkungan Mahkamah Agung f. Sudah melakukan monitoring dan evaluasi terhadap penerapan budaya kerja\"', 99, '2020-11-11 08:07:12'),
(33, 1, 'Implementasi 5R ( Checklist 5R )', 99, '2020-11-11 08:07:12'),
(34, 1, 'Pelaporan E-LHKPN ( Bukti Pelaporan LHKPN di SIKEP Screen Shoot E-LHKPN Diunggah di website)', 9, '2020-11-11 08:07:12'),
(35, 1, 'Pelaporan E-LHKASN ( Bukti laporan)', 9, '2020-11-11 08:07:12'),
(36, 1, 'Penetapan Majelis Hakim dan PP (SK Penetapan Majelis dan PP)', 9, '2020-11-11 08:07:12'),
(37, 1, 'Pemanfaatan Ruang Tamu Terbuka sesuai  SEMA No. 3 Tahun 2010 dan Surat Edaran Dirjen Badilum No. 1 Tahun 2012', 99, '2020-11-11 08:07:12'),
(38, 1, 'Monitoring Pelaksanaan Delegasi berdasarkan SEMA Nomor 6 tahun 2014 tentang Penanganan Pemberitahuan /Panggilan dan Surat Edaran Dirjen Nomor 5 tahun 2019 tentang Peningkatan Kepatuhan Penanganan Bantuan Panggilan/Pemberitahuan', 3, '2020-11-11 08:07:12'),
(39, 1, 'Monitoring  SEMA Nomor 1 tahun 2011 tentang Perubahan Surat Edaran Mahkamah Agung No 2 Tahun 2010 tentang Penyampaian Salinan dan Petikan Putusan', 3, '2020-11-11 08:07:12'),
(40, 1, 'Monitoring Pelaksanaan Eksekusi PN-PN diwilayah hukumnya Surat  WKMA Bidang Yudisial Nomor 59/WK.MA.Y/X/2019 tanggal 30 Oktober 2019 Perihal Eksekusi ', 3, '2020-11-11 08:07:12'),
(41, 1, 'Bimbingan Teknis untuk tenaga Teknis dan Tenaga Administrasi ( Rencana dan jadwal pada awal tahun laporan pelaksanaan)', 9, '2020-11-11 08:07:12'),
(42, 1, 'Sekretaris menyampaikan Laporan Realisasi Anggaran ( LRA ) ( Fotocopy laporan LRA ke KPT)', 11, '2020-11-11 08:07:12'),
(43, 1, 'Sekretaris melakukan rapat evaluasi anggaran per triwulan', 11, '2020-11-11 08:07:12'),
(44, 1, 'Sekretaris selaku Kuasa Pengguna Barang (KPB) membuat laporan persemester dan tahunan terhadap Barang Milik Negara (BMN)', 11, '2020-11-11 08:07:12'),
(45, 1, 'Sekretaris selaku Kuasa Pengguna Barang (KPB) mengusulkan penetapan status penggunaan BMN ke Biro Perlengkapan ', 11, '2020-11-11 08:07:12'),
(46, 1, 'Sekretaris selaku Kuasa Pengguna Barang (KPB) membuat laporan pengawasan dan pengendalian BMN ke Kantor Pelayanan Kekayaan Negara Lelang (KPKNL) ', 11, '2020-11-11 08:07:12'),
(47, 1, 'Pengisian Laporan Lembar kerja (LLK) ', 9, '2020-11-11 08:07:12'),
(48, 1, 'Penilaian Prestasi Kerja PNS ( PKP Pegawai dan SKP)', 9, '2020-11-11 08:07:12'),
(49, 1, 'Sekretaris melakukan rapat evaluasi kinerja pegawai per triwulan', 9, '2020-11-11 08:07:12'),
(50, 1, 'Pengadaan Barang dan Jasa (SK PPK SK Pejabat Pengadaan dan SK Pejabat Penerima Hasil Pekerja/PPHP)', 11, '2020-11-11 08:07:12'),
(51, 1, 'Verifikasi Pengguna terdaftar E- Court ( Monev) (verifikasi terhadap pengguna terdaftar paling lambat 3x24 jam sejak permohonan diterima Mengirimkan permberitahuan jawaban atas permohonan verfikasi yang diajukan Sudah di Monev dan ditindaklanjuti', 3, '2020-11-11 08:07:12'),
(52, 1, 'Pelaksanaan Pelayanan Terpadu Satu Pintu (PTSP) untuk mewujudkan budaya pelayanan prima sesuai SK DIRJEN BADILUM No. 77/DJU/SK/HM02.3/2/2018 3239/DJU/SK/HM02.3/11/2019 dan 1586/DJU/SK/PS01/9/2015 ( Monev PTSP Buku Pengawasan )', 99, '2020-11-11 08:07:12'),
(53, 1, 'seluruh temuan hasil pemeriksaan eksternal dan internal sudah ditindaklanjuti ( Laporan tindak lanjut hasil temuan eksternal dan internal)', 3, '2020-11-11 08:07:12'),
(54, 1, 'Sistem Pengendalian Intern Pemerintah/ SPIP (Lingkungan Pengendalian) ( SK SPIP Monev)', 99, '2020-11-11 08:07:12'),
(55, 1, 'Sistem Pengendalian Intern Pemerintah/ SPIP (Manajemen Resiko) ( SK MR Penetapan MR Dokuemn MR Monev)', 99, '2020-11-11 08:07:12'),
(56, 1, 'Sistem Pengendalian Intern Pemerintah/ SPIP (Pemantauan Pengendalian Intern/ Monitoring Evaluasi) ( Monev Setiap bulan)', 99, '2020-11-11 08:07:12'),
(57, 1, '\"Koordinator Operasional (Membuat Laporan kebijakan operasional (penganggaran) proses penilaian tim APM Membuat program atau jadwal pelaksanaan APM pada awal tahun Membuat laporan kepada ketua tim APM tentang kegiatan operasional secara periodik\"', 99, '2020-11-11 08:07:12'),
(58, 1, 'Koordinator Tehnikal ( BA penentuan assesor monev seluruh kegiatan assesmen APM Merencanakan dan membuat program assesmen pada awal tahun dan mengevaluasi keefektifannya Melakukan monitoring pelatihan dan evaluasi secara periodik terhadap assesor Melakukan review terhadap hasil assesmen APM yang dilaksanakan oleh assesor Membuat laporan kepada ketua tim APM tentang kegiatan tehnikal secara periodik', 99, '2020-11-11 08:07:12'),
(59, 1, '\"Quality Management Representative (QMR) (monitoring dokumen secara berkala Melakukan evaluasi untuk menjamin dan memastikan mutu dari proses APM Membut laporan kepada Ketua Tim APM tentang kinerja tim APM dan kebutuhan apapun untuk dikoreksi/perbaikan\"', 99, '2020-11-11 08:07:12'),
(60, 1, 'Tugas Document Control/ Pengendalian Dokumen (DC) (laporan kepada QMR tentang kegiatan dokumentasi tim APM)', 99, '2020-11-11 08:07:12'),
(61, 1, 'KELENGKAPAN DOCUMENT PADA DOCUMENT CONTROL', 99, '2020-11-11 08:07:12'),
(62, 1, '\"Proses Assesmen internal (SK Assesmen jadwal Assesmen dalam 1 tahun rapat persiapan pelaksanaan Assesmen Internal rencana pelaksanaan untuk setiap Assesmen Internal (yang memuat waktu pelaksanaan bagian yang diassesmennama Assesor yang melaksanakan Assesmen)   ada daftar/list pertanyaan Assesmen internal Assesmen Internal sudah dilakukan secara silangTindak lanjut sudah dimonev Bukti RTM \"', 99, '2020-11-11 08:07:12'),
(63, 2, 'Hakim Bertanggung Jawab Terhadap Minutasi Perkara (LHP Buku Pengawasan Bidang dan Notulen Rapat)', 0, '2020-11-11 08:07:12'),
(64, 2, 'Kewajiban Hakim untuk memonitor Berita Acara Sidang ( Monev)', 0, '2020-11-11 08:07:12'),
(65, 2, 'Hakim menetapkan hari sidang pertama perkara perdata dan pidana serta penetapan penahanan dan perpanjangan penahanan dalam perkara pidana ( SS SIPP)', 0, '2020-11-11 08:07:13'),
(66, 3, '\"Prosedur tata kelola arsip (lakukan uji petik minimal 3 berkas) (Penataan berkasSudah menggunakan aplikasi SIPP Tata Cara prosedur peminjaman berkas jadwal perawatan arsip \"', 0, '2020-11-11 08:07:13'),
(67, 3, 'Buku/lembar pengawas bidang pada panmud hukum', 0, '2020-11-11 08:07:13'),
(68, 3, 'Pencatatan surat masuk/keluar ( Aplikasi PTSP)', 0, '2020-11-11 08:07:13'),
(69, 3, 'Adanya uraian Tugas ( Ada Job desc dan monev)', 0, '2020-11-11 08:07:13'),
(70, 3, 'Implementasi SOP ( Monev SOP)', 0, '2020-11-11 08:07:13'),
(71, 3, 'Pengisian SIPP ( Screen Shoot SIPP)', 0, '2020-11-11 08:07:13'),
(72, 3, 'Implementasi 5R ( Checklist 5R )', 0, '2020-11-11 08:07:13'),
(73, 3, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:13'),
(74, 3, 'Prosedur Peminjaman Berkas ( Aturannya dan Bukti peminjaman berkas)', 0, '2020-11-11 08:07:13'),
(75, 3, 'Menghimpun pengaduan dan Pelayanan Masyarakat ( Monev Pengaduan dan Pelayanan Masyarakat)', 0, '2020-11-11 08:07:13'),
(76, 3, 'Pelaporan perkara secara elektronik (Monev)', 0, '2020-11-11 08:07:13'),
(77, 3, '\"Survey kepuasan masyarakat berdasarkan PERMENPAN No. 14 Tahun 2017 (SK tim survey Jadwal Survey (minimal 4 kali setahun) Kuesioner sesuai dengan tabel penilaian Ada kolom saran dan kritik pada lembar kuesioner analisa dan laporan Hasil Survey Sudah di monitoring dan 3 unsur terendah sudah dievaluasi Hasil evaluasi sudah  ditindaklanjuti hasil survey sudah terpublikasi (website  dan di ruang PTSP) \"', 0, '2020-11-11 08:07:13'),
(78, 3, 'Pelaksanaan Survey Persepsi Korupsi PERMENPAN No. 14 Tahun 2017 ((SK tim survey Jadwal Survey (minimal 4 kali setahun) Kuesioner sesuai dengan tabel penilaian Ada kolom saran dan kritik pada lembar kuesioner analisa dan laporan Hasil Survey Sudah di monitoring dan 3 unsur terendah sudah dievaluasi Hasil evaluasi sudah  ditindaklanjuti hasil survey sudah terpublikasi (website  dan di ruang PTSP)', 0, '2020-11-11 08:07:13'),
(79, 4, 'Pencatatan surat masuk/keluar (Aplikasi PTSP)', 0, '2020-11-11 08:07:13'),
(80, 4, 'Adanya uraian Tugas masing-masing unit ( Ada Job desc dan monev)', 0, '2020-11-11 08:07:13'),
(81, 4, 'Implementasi SOP ( Monev SOP)', 0, '2020-11-11 08:07:13'),
(82, 4, 'Pengisian SIPP ( SS SIPP)', 0, '2020-11-11 08:07:13'),
(83, 4, 'Buku/lembar pengawas bidang pada panmud perdata', 0, '2020-11-11 08:07:13'),
(84, 4, 'Pengiriman Putusan dan berkas perkara  dikirim ke pengadilan pengaju ( Monev)', 0, '2020-11-11 08:07:13'),
(85, 4, 'Pemberkasan Arsip Perkara yang telah diminutasi sudah sesuai denga SK Direktur Jenderal Badan Peradilan Umum No. 1939/DJU/SK/HM.02.3/10/2018 ( Monev)', 0, '2020-11-11 08:07:13'),
(86, 4, 'Penomoran perkara dan template putusan sudah sesuai dengan SK KMA No. 44 Tahun 2014', 0, '2020-11-11 08:07:13'),
(87, 4, 'Penyerahan berkas perkara inactive dari Panmud Perdata kepada Panmud Hukum harus dengan Berita Acara Serah Terima Berkas ( dalam jangka waktu 3 hari setelah BHT )', 0, '2020-11-11 08:07:13'),
(88, 4, '\"KOMDANAS Biaya Perkara  ( SS Komdanas Monev)\"', 0, '2020-11-11 08:07:13'),
(89, 4, 'Implementasi  5R ( Check lis 5R)', 0, '2020-11-11 08:07:13'),
(90, 4, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:13'),
(91, 5, 'Pencatatan surat masuk/keluar ( Aplikasi PTSP)', 0, '2020-11-11 08:07:13'),
(92, 5, 'Adanya uraian Tugas ( Ada Job desc dan monev)', 0, '2020-11-11 08:07:14'),
(93, 5, 'Implementasi SOP ( Monev SOP)', 0, '2020-11-11 08:07:14'),
(94, 5, 'Pengisian SIPP ( SS SIPP)', 0, '2020-11-11 08:07:14'),
(95, 5, 'Buku/lembar pengawas bidang pada panmud pidana', 0, '2020-11-11 08:07:14'),
(96, 5, 'Pengiriman Putusan dan berkas perkara  dikirim ke pengadilan pengaju ( Monev)', 0, '2020-11-11 08:07:14'),
(97, 5, 'Pemberkasan Arsip Perkara yang telah diminutasi sudah sesuai denga SK Direktur Jenderal Badan Peradilan Umum No. 1939/DJU/SK/HM.02.3/10/2018 ( Monev)', 0, '2020-11-11 08:07:14'),
(98, 5, 'Penomoran perkara dan template putusan sudah sesuai dengan SK KMA No. 44 Tahun 2014 (Monev)', 0, '2020-11-11 08:07:14'),
(99, 5, 'Penyerahan berkas perkara inactive dari Panmud Pidana kepada Panmud Hukum harus dengan Berita Acara Serah Terima Berkas ( dalam jangka waktu 3 hari setelah BHT ) (SS SIPP)', 0, '2020-11-11 08:07:14'),
(100, 5, 'Implementasi RINGKAS 5R', 0, '2020-11-11 08:07:14'),
(101, 5, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:14'),
(102, 6, 'Pencatatan surat masuk/keluar ( Aplikasi PTSP)', 0, '2020-11-11 08:07:14'),
(103, 6, 'Adanya uraian Tugas ( Ada Job desc dan monev)', 0, '2020-11-11 08:07:14'),
(104, 6, 'Implementasi SOP ( Monev SOP)', 0, '2020-11-11 08:07:14'),
(105, 6, 'Pengisian SIPP ( SS SIPP)', 0, '2020-11-11 08:07:14'),
(106, 6, 'Buku/lembar pengawas bidang pada panmud tipikor', 0, '2020-11-11 08:07:14'),
(107, 6, 'Pengiriman Putusan dan berkas perkara  dikirim ke pengadilan pengaju ( Monev)', 0, '2020-11-11 08:07:14'),
(108, 6, 'Pemberkasan Arsip Perkara yang telah diminutasi sudah sesuai denga SK Direktur Jenderal Badan Peradilan Umum No. 1939/DJU/SK/HM.02.3/10/2018 ( Monev)', 0, '2020-11-11 08:07:14'),
(109, 6, 'Penomoran perkara dan template putusan sudah sesuai dengan SK KMA No. 44 Tahun 2014 ( Monev)', 0, '2020-11-11 08:07:14'),
(110, 6, '\"Penyerahan berkas perkara inactive dari Panmud Pidana kepada Panmud Hukum harus dengan Berita Acara Serah Terima Berkas ( dalam jangka waktu 3 hari setelah BHT ) ( Monev) \"', 0, '2020-11-11 08:07:14'),
(111, 6, 'Implementasi 5R ( Check List 5R)', 0, '2020-11-11 08:07:14'),
(112, 6, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:14'),
(113, 7, 'Mengerjakan minutasi perkara sesuai SOP ( Monev)', 0, '2020-11-11 08:07:14'),
(114, 7, 'Implementasi 5R ( Check List 5R)', 0, '2020-11-11 08:07:14'),
(115, 9, 'Uraian Tugas masing-masing unit ( Jobdesc dan Monev)', 0, '2020-11-11 08:07:14'),
(116, 9, 'Implementasi SOP ( Monev)', 0, '2020-11-11 08:07:14'),
(117, 9, 'Pelaksanaan absensi sesuai dengan Perma No. 7 Tahun 2016 dan SK KMA 071/KMA/SK/V/2008 (uji petik tiga bulan terakhir) ( Absensi Monev Absen)', 0, '2020-11-11 08:07:14'),
(118, 9, 'Izin keluar kantor menggunakan formulir sesuai Perma No. 7 Tahun 2016 dan SK KMA 071/KMA/SK/V/2008 atau surat tugas sesuai ketentuan (uji petik tiga bulan terakhir). ( Monev)', 0, '2020-11-11 08:07:14'),
(119, 9, 'Peta kekuatan pegawai rencana kebutuhan pegawai dan Daftar Urut Kepangkatan', 0, '2020-11-11 08:07:14'),
(120, 9, 'Baperjakat dan penempatan pegawai sudah sesuai dengan kompetensi  ( SK Tim Baperjakat Rapat Baperjakat)', 0, '2020-11-11 08:07:14'),
(121, 9, '\"Analisa pengembangan kompetensi (Training Need Analysis Persentase kesenjangan kompetensi pegawai di awal tahun ST Pegawai  diklat maupun pengembangan kompetensi lainnya Laporan Bimtek Monev\"', 0, '2020-11-11 08:07:14'),
(122, 9, 'Arsip kepegawaian ( Monev)', 0, '2020-11-11 08:07:14'),
(123, 9, 'Pengelolaan SIKEP ( SS SIKEP)', 0, '2020-11-11 08:07:14'),
(124, 9, 'RKP RKGB dan usul pensiun (RKP  sudah dibuat untuk periode 1 tahun RKGB seluruhnya sudah dibuat untuk periode 1 tahun)RKP RKGB dan usul pensiun sudah terinformsikan (melalui papan atau monitor) ', 0, '2020-11-11 08:07:15'),
(125, 9, 'Pencatatan surat masuk/keluar (Aplikasi PTSP)', 0, '2020-11-11 08:07:15'),
(126, 9, 'Pemberian sanksi dan penghargaan (SK Pimpinan Berita Acara  pengajuan kenaikan pangkat satya lencana)', 0, '2020-11-11 08:07:15'),
(127, 9, 'Sasaran Kerja Pegawai (SKP) dan penilaian prestasi kerja ', 0, '2020-11-11 08:07:15'),
(128, 9, 'Prosedur izin (keluar negeribelajartugas belajar dan cuti) ( Bukti pengajuan cuti )', 0, '2020-11-11 08:07:15'),
(129, 9, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:15'),
(130, 9, '\"Pengelolaan tenaga honorer (SK dan Penilaian Honorer secara periodik) \"', 0, '2020-11-11 08:07:15'),
(131, 9, 'Implementasi 5R ( Check List 5R)', 0, '2020-11-11 08:07:15'),
(132, 9, 'Perawatan dan Pengelolaan sistem TI di pengadilan ( Jadwal perawatan Server dll)', 0, '2020-11-11 08:07:15'),
(133, 9, 'Keamanan data dan kelancaran proses sinkronisasi database SIPP dari PN ke PT ( Monev )', 0, '2020-11-11 08:07:15'),
(134, 9, '\"Standarisasi website pengadilan (Surat Dirjen Badilum No.362/DJU/HM.02.3/IV/2015 (SS Website) Menindaklanjuti Pedoman Rancangan dan Prinsip Aksebilitas Website Pengadilan oleh Ketua MA-RI pada tanggal 17 Maret 2015)\"', 0, '2020-11-11 08:07:15'),
(135, 8, '\"Implementasi aplikasi SMART (SS Aplikasi SMART) \"', 0, '2020-11-11 08:07:15'),
(136, 8, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:15'),
(137, 8, 'Implementasi 5R ( Check List 5 R)', 0, '2020-11-11 08:07:15'),
(138, 11, 'Penandatanganan Buku Kas Umum setiap akhir bulannya dilakukan oleh sekertaris dengan memastikan uang yang ada pada Brankas sesuai dengan Register Kas ( BKU Buku Bantu dan Cash Opname)', 0, '2020-11-11 08:07:15'),
(139, 11, 'Pencatatan surat masuk/keluar ( Aplikasi PTSP)', 0, '2020-11-11 08:07:15'),
(140, 11, 'Uraian Tugas masing-masing unit ( JobDesc dan Monev)', 0, '2020-11-11 08:07:15'),
(141, 11, 'Implementasi SOP ( Monev SOP)', 0, '2020-11-11 08:07:15'),
(142, 10, 'SK Pengelola BMN dan Laporan inventaris barang milik negara (SIMAK BMN) ', 0, '2020-11-11 08:07:15'),
(143, 10, 'SK Pemegang Rumah Dinas', 0, '2020-11-11 08:07:15'),
(144, 10, 'SK Kendaraan Dinas', 0, '2020-11-11 08:07:15'),
(145, 10, 'SK Perangkat IT', 0, '2020-11-11 08:07:15'),
(146, 10, 'Daftar barang ruangan & Labelisasi barang milik negara', 0, '2020-11-11 08:07:15'),
(147, 10, 'Layout jalur keluar masuk kendaraan & Pengaturan lahan parkir', 0, '2020-11-11 08:07:15'),
(148, 10, 'Jalur masuk gedung pengadilan', 0, '2020-11-11 08:07:16'),
(149, 10, 'Jalur evakuasi dan titik kumpul', 0, '2020-11-11 08:07:16'),
(150, 10, 'Simulasi tanggap darurat dan kebakaran dengan instansi terkait ( SK dan Kegiatan Simulasi)', 0, '2020-11-11 08:07:16'),
(151, 10, 'Fasilitas untuk penyandang difabel', 0, '2020-11-11 08:07:16'),
(152, 10, 'Sarana alat pemadam api ringan (APAR) ( Foto jadwal pengantian nya)', 0, '2020-11-11 08:07:16'),
(153, 10, 'Screen Shoot CCTV', 0, '2020-11-11 08:07:16'),
(154, 10, 'Petugas keamanan ( Bukti kompetensi )', 0, '2020-11-11 08:07:16'),
(155, 10, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:16'),
(156, 11, 'Sarana monitoring realisasi anggaran DIPA 01 dan DIPA 03 ( Papan LRA)', 0, '2020-11-11 08:07:16'),
(157, 11, 'Transparansi ( keterbukaan ) RKAK/L ( Diunggah di website dan di Screen shhot di papan pengumuman)', 0, '2020-11-11 08:07:16'),
(158, 11, '\"Buku-buku keuangan a.  Buku kas umum (dilengkapi dengan LPJ/akhir bulan) b.  Buku bank c.  Buku bantu (Pengawasan kredit uang persediaan SPM dan penyetoran pajak PPH Pasal 21 22 dan 23)\"', 0, '2020-11-11 08:07:16'),
(159, 11, '\"SK manajemen pengelolaan keuangan :  1. SK KPA  2. SK Pejabat Pembuat Komitmen 3. SK bendahara pengeluaran/ bendahara     pemegang uang muka  4. Bendahara penerima  5. SK Pembantu Pengelola Keuangan\"', 0, '2020-11-11 08:07:16'),
(160, 11, 'Dokumen pertanggung jawaban telah disimpan dan diarsipkan ( Foto lemari arsip)', 0, '2020-11-11 08:07:16'),
(161, 10, 'Barang Persediaan Dipa 01 dan Dipa 03 ( Laporan Persediaan perbulan)', 0, '2020-11-11 08:07:16'),
(162, 10, 'Prosedur Pendistribusian Barang (Berita Acara Serah Terima Barang dan Tanda Terima Barang)', 0, '2020-11-11 08:07:16'),
(163, 11, 'Monitoring uang persediaan di brankas bendahara oleh KPA setiap bulan ( Cash Opname)', 0, '2020-11-11 08:07:16'),
(164, 11, 'Satker sudah menginput aplikasi monev smart setiap bulannya dan melaporkan kepada pimpinan. (SS Aplikasi SMART)', 0, '2020-11-11 08:07:16'),
(165, 11, 'Dokumentasi Rapat/ Notulen Rapat', 0, '2020-11-11 08:07:16'),
(166, 11, 'Satker sudah menginput aplikasi monev PP 39 Tahun 2006 dari Bapenas setiap triwulan dan melaporkan kepada pimpinan (Laporan Monev Bappenas)', 0, '2020-11-11 08:07:16'),
(167, 11, 'Satker telah melakukan Rekon internal antara aplikasi SIMAK BMN SAIBA dan KOMDANAS setiap bulannya dengan membuat berita acara Rekon internal (mengetahui : operator SAIBA operator SIMAK BMN dan KPA) ( BA Rekonsiliasi)', 0, '2020-11-11 08:07:16'),
(168, 10, 'Kontrak kantin (PNBP Kantin)', 0, '2020-11-11 08:07:16'),
(169, 11, 'Sistem Informasi Rencana Umum Pengadaan (SIRUP)', 0, '2020-11-11 08:07:16'),
(170, 10, 'Implementasi 5R ( Check List 5R)', 0, '2020-11-11 08:07:16'),
(171, 11, 'Pelaporan Keuangan satker DIPA 01 dan 03  (berdasarkan PMK no.22/PMK.05/2016) sesuai dengan standard akutansi pemerintah yang berlaku.  ( CALK 01 dan 03)', 0, '2020-11-11 08:07:16');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(48) NOT NULL,
  `hak_akses` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `nama`, `hak_akses`) VALUES
(1, 'Superadmin', 'Superadmin'),
(2, 'Top Manajemen', 'Top Manajemen'),
(3, 'QMR', 'QMR'),
(4, 'Hakim', 'Hakim'),
(5, 'Kabag', 'Kabag'),
(6, 'User', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nip_pegawai` varchar(18) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` varchar(64) NOT NULL,
  `role` int(11) NOT NULL,
  `blokir` varchar(10) NOT NULL,
  `date_updated` date NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nip_pegawai`, `username`, `password`, `email`, `role`, `blokir`, `date_updated`, `date_created`) VALUES
(1, '199807202022061025', 'admin', 'admin', 'admin@ad.com', 1, 'tidak', '2020-11-06', '2020-10-26 21:48:26'),
(2, '123456789987654321', 'ketua', 'ketua', 'ketua@hc.com', 2, 'tidak', '2020-10-26', '2020-10-26 22:26:27'),
(3, '987456321654789123', 'wakilketua', 'wakilketua', 'wakil@hc.com', 3, 'tidak', '2020-10-26', '2020-10-26 22:27:07'),
(4, '123456789654321978', 'hakim', 'hakim', 'hakim@hc.com', 4, 'tidak', '2020-10-26', '2020-10-26 22:33:32'),
(5, '123654879458796321', 'panmudpidana', 'panmudpidana', 'pmpidana@hc.com', 6, 'tidak', '2020-10-26', '2020-10-26 22:37:33'),
(6, '153624789789456123', 'panmudperdata', 'panmudperdata', 'panmudperdata@hc.com', 6, 'tidak', '2020-10-26', '2020-10-26 22:38:43'),
(7, '953153759789456123', 'panmudtipikor', 'panmudtipikor', 'panmudtipikor@email.com', 6, 'tidak', '2020-10-26', '2020-10-26 22:45:08'),
(8, '458796369456789123', 'umumnkeuangan', 'umumnkeuangan', 'umumnkeuangan@test.com', 6, 'tidak', '2020-10-26', '2020-10-26 22:46:19'),
(10, '452178963369852147', 'pkp', 'pkp', 'pkp@test.com', 5, 'tidak', '2020-10-26', '2020-10-26 23:17:09'),
(12, '456965874123456789', 'tedt', 'tedt', 'a@a.c', 5, 'tidak', '2020-11-02', '2020-11-02 05:31:44'),
(13, '18237130', 'muhammadaliyya19', 'test', 'aku@tester.comn', 6, 'tidak', '2020-11-06', '2020-11-05 09:44:51'),
(15, '456957896321456789', 'nurha', 'nurha', 'nurha@g.c', 6, 'tidak', '2020-11-11', '2020-11-11 15:38:40'),
(16, '582936714456789123', 'srdiang', 'srdiang', 'sdg@g.com', 6, 'tidak', '2020-11-11', '2020-11-11 15:50:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evidence`
--
ALTER TABLE `evidence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`NIP`),
  ADD KEY `NIP_2` (`NIP`);

--
-- Indexes for table `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `evidence`
--
ALTER TABLE `evidence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
